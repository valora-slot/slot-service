DELETE FROM reservation
WHERE end_date_time <
	(SELECT now() AT TIME ZONE 'utc');