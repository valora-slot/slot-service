package dev.valora.slotservice.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import dev.valora.slotservice.model.SlotTemplate;

public interface SlotTemplateRepository
		extends CrudRepository<SlotTemplate, UUID>, JpaSpecificationExecutor<SlotTemplate> {}
