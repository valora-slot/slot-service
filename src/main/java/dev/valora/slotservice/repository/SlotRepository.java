package dev.valora.slotservice.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import dev.valora.slotservice.model.Slot;

public interface SlotRepository
		extends CrudRepository<Slot, String>, JpaSpecificationExecutor<Slot> {}
