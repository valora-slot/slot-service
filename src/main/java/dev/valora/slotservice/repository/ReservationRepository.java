package dev.valora.slotservice.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import dev.valora.slotservice.model.Reservation;

public interface ReservationRepository
		extends CrudRepository<Reservation, UUID>, JpaSpecificationExecutor<Reservation> {}
