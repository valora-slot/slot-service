package dev.valora.slotservice.repository;

import org.springframework.data.repository.CrudRepository;

import dev.valora.slotservice.model.BookableResource;

public interface BookableResourceRepository
		extends CrudRepository<BookableResource, String> {}
