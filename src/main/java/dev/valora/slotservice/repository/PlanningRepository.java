package dev.valora.slotservice.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import dev.valora.slotservice.model.Planning;

public interface PlanningRepository
		extends CrudRepository<Planning, UUID>, JpaSpecificationExecutor<Planning> {}
