package dev.valora.slotservice.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import dev.valora.slotservice.model.PlanningTemplate;

public interface PlanningTemplateRepository
		extends PagingAndSortingRepository<PlanningTemplate, UUID>, JpaSpecificationExecutor<PlanningTemplate> {}
