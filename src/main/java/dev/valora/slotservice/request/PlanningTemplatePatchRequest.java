package dev.valora.slotservice.request;

import java.time.ZonedDateTime;

public class PlanningTemplatePatchRequest {

	private ZonedDateTime startDateTime;

	private ZonedDateTime endDateTime;

	public ZonedDateTime getStartDateTime() {
		return startDateTime;
	}

	public ZonedDateTime getEndDateTime() {
		return endDateTime;
	}

}
