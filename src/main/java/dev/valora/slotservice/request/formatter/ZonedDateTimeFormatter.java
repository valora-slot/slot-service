package dev.valora.slotservice.request.formatter;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.format.Formatter;

public class ZonedDateTimeFormatter
		implements Formatter<ZonedDateTime> {

	@Override
	public ZonedDateTime parse(String text, Locale locale) throws ParseException {
		return parse(text);
	}

	@Override
	public String print(ZonedDateTime object, Locale locale) {
		return DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(object);
	}

	public ZonedDateTime parse(String text) {
		return ZonedDateTime.parse(text, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
	}

}
