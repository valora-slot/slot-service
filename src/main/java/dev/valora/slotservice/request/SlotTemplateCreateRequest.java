package dev.valora.slotservice.request;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.UUID;

import javax.validation.constraints.NotNull;

public class SlotTemplateCreateRequest {

	@NotNull
	private DayOfWeek dayOfWeek;

	@NotNull
	private LocalTime startTime;

	@NotNull
	private LocalTime endTime;

	@NotNull
	private Integer maxCapacity;

	@NotNull
	private UUID planningTemplateId;

	public DayOfWeek getDayOfWeek() {
		return dayOfWeek;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public Integer getMaxCapacity() {
		return maxCapacity;
	}

	public UUID getPlanningTemplateId() {
		return planningTemplateId;
	}

}
