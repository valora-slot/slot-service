package dev.valora.slotservice.request;

import javax.validation.constraints.NotNull;

public class PlanningCreateRequest {

	private String label;

	@NotNull
	private String bookableResourceId;

	public String getLabel() {
		return label;
	}

	public String getBookableResourceId() {
		return bookableResourceId;
	}

}
