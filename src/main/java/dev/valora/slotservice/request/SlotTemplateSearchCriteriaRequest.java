package dev.valora.slotservice.request;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import dev.valora.slotservice.model.PlanningTemplate;
import dev.valora.slotservice.model.SlotTemplate;

public class SlotTemplateSearchCriteriaRequest
		implements Specification<SlotTemplate> {

	private static final long serialVersionUID = 8184582641186123089L;

	private UUID planningTemplateId;

	public void setPlanningTemplateId(UUID planningTemplateId) {
		this.planningTemplateId = planningTemplateId;
	}

	@Override
	public Predicate toPredicate(Root<SlotTemplate> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		Collection<Predicate> restrictions = new ArrayList<>();

		if (planningTemplateId != null) {
			Predicate planningPredicate = criteriaBuilder.equal(root.<PlanningTemplate> get("planningTemplate").<UUID> get("id"), planningTemplateId);
			restrictions.add(planningPredicate);
		}

		return criteriaBuilder.and(restrictions.stream().toArray(Predicate[]::new));
	}

}
