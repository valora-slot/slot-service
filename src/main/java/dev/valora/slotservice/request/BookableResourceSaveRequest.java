package dev.valora.slotservice.request;

import java.time.ZoneId;

import javax.validation.constraints.NotNull;

public class BookableResourceSaveRequest {

	private String label;

	@NotNull
	private ZoneId timeZone;

	public String getLabel() {
		return label;
	}

	public ZoneId getTimeZone() {
		return timeZone;
	}

}
