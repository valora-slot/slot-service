package dev.valora.slotservice.request;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import dev.valora.slotservice.model.Reservation;
import dev.valora.slotservice.model.Slot;

public class ReservationSearchCriteriaRequest
		implements Specification<Reservation> {

	private static final long serialVersionUID = -7295419809193803866L;

	private String slotId;
	private String owner;

	public void setSlotId(String slotId) {
		this.slotId = slotId;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	@Override
	public Predicate toPredicate(Root<Reservation> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		Collection<Predicate> restrictions = new ArrayList<>();

		if (slotId != null) {
			Predicate slotPredicate = criteriaBuilder.equal(root.<Slot> get("slot").<String> get("id"), slotId);
			restrictions.add(slotPredicate);
		}

		if (owner != null) {
			Predicate ownerPredicate = criteriaBuilder.equal(root.<String> get("owner"), owner);
			restrictions.add(ownerPredicate);
		}

		return criteriaBuilder.and(restrictions.stream().toArray(Predicate[]::new));
	}

}
