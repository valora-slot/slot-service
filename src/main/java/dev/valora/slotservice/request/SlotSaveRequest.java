package dev.valora.slotservice.request;

import java.time.ZonedDateTime;
import java.util.UUID;

import javax.validation.constraints.NotNull;

public class SlotSaveRequest {

	@NotNull
	private ZonedDateTime startDateTime;

	@NotNull
	private ZonedDateTime endDateTime;

	@NotNull
	private Integer maxCapacity;

	@NotNull
	private UUID planningId;

	public ZonedDateTime getStartDateTime() {
		return startDateTime;
	}

	public ZonedDateTime getEndDateTime() {
		return endDateTime;
	}

	public Integer getMaxCapacity() {
		return maxCapacity;
	}

	public UUID getPlanningId() {
		return planningId;
	}

}
