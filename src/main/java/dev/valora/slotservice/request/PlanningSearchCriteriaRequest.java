package dev.valora.slotservice.request;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import dev.valora.slotservice.model.BookableResource;
import dev.valora.slotservice.model.Planning;

public class PlanningSearchCriteriaRequest
		implements Specification<Planning> {

	private static final long serialVersionUID = -6105206815479968735L;

	private String bookableResourceId;

	public void setBookableResourceId(String bookableResourceId) {
		this.bookableResourceId = bookableResourceId;
	}

	@Override
	public Predicate toPredicate(Root<Planning> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		Collection<Predicate> restrictions = new ArrayList<>();

		if (bookableResourceId != null) {
			Predicate bookableResourcePredicate = criteriaBuilder.equal(root.<BookableResource> get("bookableResource").<String> get("id"),
					bookableResourceId);
			restrictions.add(bookableResourcePredicate);
		}

		return criteriaBuilder.and(restrictions.stream().toArray(Predicate[]::new));
	}

}
