package dev.valora.slotservice.request;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.Specification;

import dev.valora.slotservice.model.BookableResource;
import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.model.Slot;

public class SlotSearchCriteriaRequest
		implements Specification<Slot> {

	private static final long serialVersionUID = 7311239775955722503L;

	private String bookableResourceId;
	private UUID planningId;

	@NotNull
	private ZonedDateTime startDateTimeFrom;

	@NotNull
	private ZonedDateTime startDateTimeTo;

	@AssertTrue(message = "{dev.valora.slotservice.request.SlotSearchCriteriaRequest.message}")
	private boolean isValid() {
		return planningId != null || bookableResourceId != null;
	}

	public UUID getPlanningId() {
		return planningId;
	}

	public void setPlanningId(UUID planningId) {
		this.planningId = planningId;
	}

	public String getBookableResourceId() {
		return bookableResourceId;
	}

	public void setBookableResourceId(String bookableResourceId) {
		this.bookableResourceId = bookableResourceId;
	}

	public ZonedDateTime getStartDateTimeFrom() {
		return startDateTimeFrom;
	}

	public void setStartDateTimeFrom(ZonedDateTime startDateTimeFrom) {
		this.startDateTimeFrom = startDateTimeFrom;
	}

	public ZonedDateTime getStartDateTimeTo() {
		return startDateTimeTo;
	}

	public void setStartDateTimeTo(ZonedDateTime startDateTimeTo) {
		this.startDateTimeTo = startDateTimeTo;
	}

	@Override
	public Predicate toPredicate(Root<Slot> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		Collection<Predicate> restrictions = new ArrayList<>();

		if (planningId != null) {
			Predicate planningPredicate = criteriaBuilder.equal(root.<Planning> get("planning").<UUID> get("id"), planningId);
			restrictions.add(planningPredicate);
		}

		if (bookableResourceId != null) {
			Predicate bookableResourcePredicate = criteriaBuilder
					.equal(root.<Planning> get("planning").<BookableResource> get("bookableResource").<String> get("id"), bookableResourceId);
			restrictions.add(bookableResourcePredicate);
		}

		if (startDateTimeFrom != null && startDateTimeTo != null) {
			Predicate startDateTimePredicate = criteriaBuilder.between(root.<ZonedDateTime> get("startDateTime"), startDateTimeFrom, startDateTimeTo);
			restrictions.add(startDateTimePredicate);
		}

		return criteriaBuilder.and(restrictions.stream().toArray(Predicate[]::new));
	}

}
