package dev.valora.slotservice.request;

import java.time.ZonedDateTime;

import javax.validation.constraints.NotNull;

public class ReservationSaveRequest {

	@NotNull
	private String slotId;

	private String owner;

	private ZonedDateTime endDateTime;

	public String getSlotId() {
		return slotId;
	}

	public String getOwner() {
		return owner;
	}

	public ZonedDateTime getEndDateTime() {
		return endDateTime;
	}

}
