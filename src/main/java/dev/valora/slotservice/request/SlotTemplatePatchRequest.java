package dev.valora.slotservice.request;

import java.time.DayOfWeek;
import java.time.LocalTime;

public class SlotTemplatePatchRequest {

	private DayOfWeek dayOfWeek;
	private LocalTime startTime;
	private LocalTime endTime;
	private Integer maxCapacity;

	public DayOfWeek getDayOfWeek() {
		return dayOfWeek;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public Integer getMaxCapacity() {
		return maxCapacity;
	}

}
