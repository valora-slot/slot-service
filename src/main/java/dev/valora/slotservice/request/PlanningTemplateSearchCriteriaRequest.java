package dev.valora.slotservice.request;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import dev.valora.slotservice.model.BookableResource;
import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.model.PlanningTemplate;

public class PlanningTemplateSearchCriteriaRequest
		implements Specification<PlanningTemplate> {

	private static final long serialVersionUID = -6105206815479968735L;

	private UUID planningId;
	private String bookableResourceId;
	private ZonedDateTime activeDateTimeFrom;
	private ZonedDateTime activeDateTimeTo;

	public void setPlanningId(UUID planningId) {
		this.planningId = planningId;
	}

	public void setBookableResourceId(String bookableResourceId) {
		this.bookableResourceId = bookableResourceId;
	}

	public void setActiveDateTimeFrom(ZonedDateTime activeDateTimeFrom) {
		this.activeDateTimeFrom = activeDateTimeFrom;
	}

	public void setActiveDateTimeTo(ZonedDateTime activeDateTimeTo) {
		this.activeDateTimeTo = activeDateTimeTo;
	}

	@Override
	public Predicate toPredicate(Root<PlanningTemplate> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		Collection<Predicate> restrictions = new ArrayList<>();

		if (planningId != null) {
			Predicate planningPredicate = criteriaBuilder.equal(root.<Planning> get("planning").<UUID> get("id"), planningId);
			restrictions.add(planningPredicate);
		}

		if (bookableResourceId != null) {
			Predicate bookableResourcePredicate = criteriaBuilder
					.equal(root.<Planning> get("planning").<BookableResource> get("bookableResource").<String> get("id"), bookableResourceId);
			restrictions.add(bookableResourcePredicate);
		}

		if (activeDateTimeFrom != null && activeDateTimeTo != null) {
			Predicate activePredicate = criteriaBuilder.and(criteriaBuilder.lessThan(root.<ZonedDateTime> get("startDateTime"), activeDateTimeTo),
					criteriaBuilder.or(criteriaBuilder.isNull(root.<ZonedDateTime> get("endDateTime")),
							criteriaBuilder.greaterThan(root.<ZonedDateTime> get("endDateTime"), activeDateTimeFrom)));
			restrictions.add(activePredicate);
		}

		return criteriaBuilder.and(restrictions.stream().toArray(Predicate[]::new));
	}

}
