package dev.valora.slotservice.request;

import java.time.ZonedDateTime;
import java.util.UUID;

import javax.validation.constraints.NotNull;

public class PlanningTemplateCreateRequest {

	@NotNull
	private ZonedDateTime startDateTime;

	private ZonedDateTime endDateTime;

	@NotNull
	private UUID planningId;

	public ZonedDateTime getStartDateTime() {
		return startDateTime;
	}

	public ZonedDateTime getEndDateTime() {
		return endDateTime;
	}

	public UUID getPlanningId() {
		return planningId;
	}

}
