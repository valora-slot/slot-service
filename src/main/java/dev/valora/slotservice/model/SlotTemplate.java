package dev.valora.slotservice.model;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Collection;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import dev.valora.commons.springsecurityjpaauditing.model.AuditingEntity;

@Entity
public class SlotTemplate
		extends AuditingEntity {

	@Id
	@GeneratedValue
	private UUID id;

	@Column(name = "day_of_week", nullable = false)
	private DayOfWeek dayOfWeek;

	@Column(name = "start_time", nullable = false)
	private LocalTime startTime;

	@Column(name = "end_time", nullable = false)
	private LocalTime endTime;

	@Column(name = "max_capacity", nullable = false)
	private Integer maxCapacity;

	@ManyToOne(optional = false)
	private PlanningTemplate planningTemplate;

	@OneToMany(mappedBy = "slotTemplate", orphanRemoval = true)
	private Collection<Slot> slots;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public DayOfWeek getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(DayOfWeek dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public Integer getMaxCapacity() {
		return maxCapacity;
	}

	public void setMaxCapacity(Integer maxCapacity) {
		this.maxCapacity = maxCapacity;
	}

	public PlanningTemplate getPlanningTemplate() {
		return planningTemplate;
	}

	public void setPlanningTemplate(PlanningTemplate planningTemplate) {
		this.planningTemplate = planningTemplate;
	}

	public Collection<Slot> getSlots() {
		return slots;
	}

	public void setSlots(Collection<Slot> slots) {
		this.slots = slots;
	}

}
