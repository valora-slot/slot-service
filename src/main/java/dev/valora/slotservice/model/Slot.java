package dev.valora.slotservice.model;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import dev.valora.commons.springsecurityjpaauditing.model.AuditingEntity;

@Entity
public class Slot
		extends AuditingEntity {

	@Id
	private String id;

	@Column(name = "start_date_time", nullable = false)
	private ZonedDateTime startDateTime;

	@Column(name = "end_date_time", nullable = false)
	private ZonedDateTime endDateTime;

	@Column(name = "max_capacity", nullable = false)
	private Integer maxCapacity;

	@ManyToOne(optional = false)
	@JoinColumn(name = "planning_id")
	private Planning planning;

	@ManyToOne
	@JoinColumn(name = "slot_template_id")
	private SlotTemplate slotTemplate;

	@OneToMany(mappedBy = "slot")
	private Collection<Reservation> reservations;

	public Slot() {
		id = UUID.randomUUID().toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ZonedDateTime getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(ZonedDateTime startDateTime) {
		this.startDateTime = startDateTime;
	}

	public ZonedDateTime getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(ZonedDateTime endDateTime) {
		this.endDateTime = endDateTime;
	}

	public Integer getMaxCapacity() {
		return maxCapacity;
	}

	public void setMaxCapacity(Integer maxCapacity) {
		this.maxCapacity = maxCapacity;
	}

	public Planning getPlanning() {
		return planning;
	}

	public void setPlanning(Planning planning) {
		this.planning = planning;
	}

	public SlotTemplate getSlotTemplate() {
		return slotTemplate;
	}

	public void setSlotTemplate(SlotTemplate slotTemplate) {
		this.slotTemplate = slotTemplate;
	}

	public Collection<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(Collection<Reservation> reservations) {
		this.reservations = reservations;
	}

	@Override
	public int hashCode() {
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
		hashCodeBuilder.append(id);
		return hashCodeBuilder.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Slot other = (Slot) obj;
		EqualsBuilder equalsBuilder = new EqualsBuilder();
		equalsBuilder.append(id, other.id);
		return equalsBuilder.isEquals();
	}

}
