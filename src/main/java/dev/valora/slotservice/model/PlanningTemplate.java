package dev.valora.slotservice.model;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import dev.valora.commons.springsecurityjpaauditing.model.AuditingEntity;

@Entity
public class PlanningTemplate
		extends AuditingEntity {

	@Id
	@GeneratedValue
	private UUID id;

	@Column(name = "start_date_time", nullable = false)
	private ZonedDateTime startDateTime;

	@Column(name = "end_date_time")
	private ZonedDateTime endDateTime;

	@ManyToOne(optional = false)
	@JoinColumn(name = "planning_id")
	private Planning planning;

	@OneToMany(mappedBy = "planningTemplate", cascade = CascadeType.ALL, orphanRemoval = true)
	private Collection<SlotTemplate> slotTemplates;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public ZonedDateTime getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(ZonedDateTime startDateTime) {
		this.startDateTime = startDateTime;
	}

	public ZonedDateTime getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(ZonedDateTime endDateTime) {
		this.endDateTime = endDateTime;
	}

	public Planning getPlanning() {
		return planning;
	}

	public void setPlanning(Planning planning) {
		this.planning = planning;
	}

	public Collection<SlotTemplate> getSlotTemplates() {
		return slotTemplates;
	}

	public void setSlotTemplates(Collection<SlotTemplate> slotTemplates) {
		this.slotTemplates = slotTemplates;
	}

}
