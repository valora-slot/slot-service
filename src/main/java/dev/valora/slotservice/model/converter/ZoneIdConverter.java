package dev.valora.slotservice.model.converter;

import java.time.ZoneId;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ZoneIdConverter
		implements AttributeConverter<ZoneId, String> {

	@Override
	public String convertToDatabaseColumn(ZoneId attribute) {
		if (attribute == null) {
			return null;
		}
		return attribute.getId();
	}

	@Override
	public ZoneId convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return null;
		}
		return ZoneId.of(dbData);
	}

}
