package dev.valora.slotservice.model.converter;

import java.time.DayOfWeek;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class DayOfWeekConverter
		implements AttributeConverter<DayOfWeek, Integer> {

	@Override
	public Integer convertToDatabaseColumn(DayOfWeek attribute) {
		if (attribute == null) {
			return null;
		}
		return attribute.getValue();
	}

	@Override
	public DayOfWeek convertToEntityAttribute(Integer dbData) {
		if (dbData == null) {
			return null;
		}
		return DayOfWeek.of(dbData.intValue());
	}

}
