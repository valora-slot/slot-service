package dev.valora.slotservice.model;

import java.time.ZoneId;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import dev.valora.commons.springsecurityjpaauditing.model.AuditingEntity;

@Entity
public class BookableResource
		extends AuditingEntity {

	@Id
	private String id;

	private String label;

	@Column(name = "time_zone", nullable = false)
	private ZoneId timeZone;

	@OneToMany(mappedBy = "bookableResource", orphanRemoval = true)
	private Collection<Planning> plannings;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public ZoneId getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(ZoneId timeZone) {
		this.timeZone = timeZone;
	}

	public Collection<Planning> getPlannings() {
		return plannings;
	}

	public void setPlannings(Collection<Planning> plannings) {
		this.plannings = plannings;
	}

}
