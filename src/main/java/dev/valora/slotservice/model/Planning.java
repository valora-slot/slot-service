package dev.valora.slotservice.model;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import dev.valora.commons.springsecurityjpaauditing.model.AuditingEntity;

@Entity
public class Planning
		extends AuditingEntity {

	@Id
	@GeneratedValue
	private UUID id;

	private String label;

	@ManyToOne(optional = false)
	@JoinColumn(name = "bookable_resource_id")
	private BookableResource bookableResource;

	@OneToMany(mappedBy = "planning", orphanRemoval = true)
	private Collection<PlanningTemplate> planningTemplates;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public BookableResource getBookableResource() {
		return bookableResource;
	}

	public void setBookableResource(BookableResource bookableResource) {
		this.bookableResource = bookableResource;
	}

	public Collection<PlanningTemplate> getPlanningTemplates() {
		return planningTemplates;
	}

	public void setPlanningTemplates(Collection<PlanningTemplate> planningTemplates) {
		this.planningTemplates = planningTemplates;
	}

}
