package dev.valora.slotservice.model;

import java.time.ZonedDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import dev.valora.commons.springsecurityjpaauditing.model.AuditingEntity;

@Entity
public class Reservation
		extends AuditingEntity {

	@Id
	@GeneratedValue
	private UUID id;

	@ManyToOne(optional = false)
	private Slot slot;

	private String owner;

	@Column(name = "end_date_time")
	private ZonedDateTime endDateTime;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Slot getSlot() {
		return slot;
	}

	public void setSlot(Slot slot) {
		this.slot = slot;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public ZonedDateTime getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(ZonedDateTime endDateTime) {
		this.endDateTime = endDateTime;
	}

}
