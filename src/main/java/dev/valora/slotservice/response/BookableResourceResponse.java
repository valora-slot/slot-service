package dev.valora.slotservice.response;

import java.time.ZoneId;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

import dev.valora.slotservice.model.BookableResource;

public class BookableResourceResponse
		extends ResourceSupport {

	private BookableResource bookableResource;

	public BookableResourceResponse(BookableResource bookableResource) {
		this.bookableResource = bookableResource;
	}

	@JsonProperty("id")
	public String getReference() {
		return bookableResource.getId();
	}

	public String getLabel() {
		return bookableResource.getLabel();
	}

	public ZoneId getTimeZone() {
		return bookableResource.getTimeZone();
	}

	@Override
	public int hashCode() {
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
		hashCodeBuilder.appendSuper(super.hashCode());
		hashCodeBuilder.append(getReference());
		hashCodeBuilder.append(getLabel());
		hashCodeBuilder.append(getTimeZone());
		return hashCodeBuilder.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		BookableResourceResponse other = (BookableResourceResponse) obj;
		EqualsBuilder equalsBuilder = new EqualsBuilder();
		equalsBuilder.appendSuper(super.equals(other));
		equalsBuilder.append(getReference(), other.getReference());
		equalsBuilder.append(getLabel(), other.getLabel());
		equalsBuilder.append(getTimeZone(), other.getTimeZone());
		return equalsBuilder.isEquals();
	}

}
