package dev.valora.slotservice.response;

import java.time.ZonedDateTime;
import java.util.UUID;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

import dev.valora.slotservice.model.Reservation;

public class ReservationResponse
		extends ResourceSupport {

	private Reservation reservation;

	public ReservationResponse(Reservation reservation) {
		this.reservation = reservation;
	}

	@JsonProperty("id")
	public UUID getReference() {
		return reservation.getId();
	}

	public String getSlotId() {
		return reservation.getSlot().getId();
	}

	public String getOwner() {
		return reservation.getOwner();
	}

	public ZonedDateTime getEndDateTime() {
		return reservation.getEndDateTime();
	}

	@Override
	public int hashCode() {
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
		hashCodeBuilder.appendSuper(super.hashCode());
		hashCodeBuilder.append(getReference());
		hashCodeBuilder.append(getSlotId());
		hashCodeBuilder.append(getOwner());
		hashCodeBuilder.append(getEndDateTime());
		return hashCodeBuilder.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		ReservationResponse other = (ReservationResponse) obj;
		EqualsBuilder equalsBuilder = new EqualsBuilder();
		equalsBuilder.appendSuper(super.equals(other));
		equalsBuilder.append(getReference(), other.getReference());
		equalsBuilder.append(getSlotId(), other.getSlotId());
		equalsBuilder.append(getOwner(), other.getOwner());
		equalsBuilder.append(getEndDateTime(), other.getEndDateTime());
		return equalsBuilder.isEquals();
	}

}
