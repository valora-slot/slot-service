package dev.valora.slotservice.response;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.UUID;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

import dev.valora.slotservice.model.SlotTemplate;

public class SlotTemplateResponse
		extends ResourceSupport {

	private SlotTemplate slotTemplate;

	public SlotTemplateResponse(SlotTemplate slotTemplate) {
		this.slotTemplate = slotTemplate;
	}

	@JsonProperty("id")
	public UUID getReference() {
		return slotTemplate.getId();
	}

	public DayOfWeek getDayOfWeek() {
		return slotTemplate.getDayOfWeek();
	}

	public LocalTime getStartTime() {
		return slotTemplate.getStartTime();
	}

	public LocalTime getEndTime() {
		return slotTemplate.getEndTime();
	}

	public Integer getMaxCapacity() {
		return slotTemplate.getMaxCapacity();
	}

	public UUID getPlanningTemplateId() {
		return slotTemplate.getPlanningTemplate().getId();
	}

	@Override
	public int hashCode() {
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
		hashCodeBuilder.appendSuper(super.hashCode());
		hashCodeBuilder.append(getReference());
		hashCodeBuilder.append(getDayOfWeek());
		hashCodeBuilder.append(getStartTime());
		hashCodeBuilder.append(getEndTime());
		hashCodeBuilder.append(getMaxCapacity());
		hashCodeBuilder.append(getPlanningTemplateId());
		return hashCodeBuilder.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		SlotTemplateResponse other = (SlotTemplateResponse) obj;
		EqualsBuilder equalsBuilder = new EqualsBuilder();
		equalsBuilder.appendSuper(super.equals(other));
		equalsBuilder.append(getReference(), other.getReference());
		equalsBuilder.append(getDayOfWeek(), other.getDayOfWeek());
		equalsBuilder.append(getStartTime(), other.getStartTime());
		equalsBuilder.append(getEndTime(), other.getEndTime());
		equalsBuilder.append(getMaxCapacity(), other.getMaxCapacity());
		equalsBuilder.append(getPlanningTemplateId(), other.getPlanningTemplateId());
		return equalsBuilder.isEquals();
	}

}
