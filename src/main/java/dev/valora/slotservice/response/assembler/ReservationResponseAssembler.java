package dev.valora.slotservice.response.assembler;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import dev.valora.slotservice.controller.ReservationController;
import dev.valora.slotservice.model.Reservation;
import dev.valora.slotservice.response.ReservationResponse;

public class ReservationResponseAssembler
		extends ResourceAssemblerSupport<Reservation, ReservationResponse> {

	public ReservationResponseAssembler() {
		super(ReservationController.class, ReservationResponse.class);
	}

	@Override
	public ReservationResponse toResource(Reservation entity) {
		return new ReservationResponse(entity);
	}

}
