package dev.valora.slotservice.response.assembler;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import dev.valora.slotservice.controller.BookableResourceController;
import dev.valora.slotservice.model.BookableResource;
import dev.valora.slotservice.response.BookableResourceResponse;

public class BookableResourceResponseAssembler
		extends ResourceAssemblerSupport<BookableResource, BookableResourceResponse> {

	public BookableResourceResponseAssembler() {
		super(BookableResourceController.class, BookableResourceResponse.class);
	}

	@Override
	public BookableResourceResponse toResource(BookableResource entity) {
		return new BookableResourceResponse(entity);
	}

}
