package dev.valora.slotservice.response.assembler;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import dev.valora.slotservice.controller.SlotTemplateController;
import dev.valora.slotservice.model.SlotTemplate;
import dev.valora.slotservice.response.SlotTemplateResponse;

public class SlotTemplateResponseAssembler
		extends ResourceAssemblerSupport<SlotTemplate, SlotTemplateResponse> {

	public SlotTemplateResponseAssembler() {
		super(SlotTemplateController.class, SlotTemplateResponse.class);
	}

	@Override
	public SlotTemplateResponse toResource(SlotTemplate entity) {
		return new SlotTemplateResponse(entity);
	}

}
