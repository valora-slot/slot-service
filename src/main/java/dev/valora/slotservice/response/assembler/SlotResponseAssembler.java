package dev.valora.slotservice.response.assembler;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import dev.valora.slotservice.controller.SlotController;
import dev.valora.slotservice.model.Slot;
import dev.valora.slotservice.response.SlotResponse;

public class SlotResponseAssembler
		extends ResourceAssemblerSupport<Slot, SlotResponse> {

	public SlotResponseAssembler() {
		super(SlotController.class, SlotResponse.class);
	}

	@Override
	public SlotResponse toResource(Slot entity) {
		return new SlotResponse(entity);
	}

}
