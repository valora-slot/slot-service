package dev.valora.slotservice.response.assembler;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import dev.valora.slotservice.controller.PlanningTemplateController;
import dev.valora.slotservice.model.PlanningTemplate;
import dev.valora.slotservice.response.PlanningTemplateResponse;

public class PlanningTemplateResponseAssembler
		extends ResourceAssemblerSupport<PlanningTemplate, PlanningTemplateResponse> {

	public PlanningTemplateResponseAssembler() {
		super(PlanningTemplateController.class, PlanningTemplateResponse.class);
	}

	@Override
	public PlanningTemplateResponse toResource(PlanningTemplate entity) {
		return new PlanningTemplateResponse(entity);
	}

}
