package dev.valora.slotservice.response.assembler;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import dev.valora.slotservice.controller.PlanningController;
import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.response.PlanningResponse;

public class PlanningResponseAssembler
		extends ResourceAssemblerSupport<Planning, PlanningResponse> {

	public PlanningResponseAssembler() {
		super(PlanningController.class, PlanningResponse.class);
	}

	@Override
	public PlanningResponse toResource(Planning entity) {
		return new PlanningResponse(entity);
	}

}
