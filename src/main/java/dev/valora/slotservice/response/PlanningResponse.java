package dev.valora.slotservice.response;

import java.util.UUID;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

import dev.valora.slotservice.model.Planning;

public class PlanningResponse
		extends ResourceSupport {

	private Planning planning;

	public PlanningResponse(Planning planning) {
		this.planning = planning;
	}

	@JsonProperty("id")
	public UUID getReference() {
		return planning.getId();
	}

	public String getLabel() {
		return planning.getLabel();
	}

	public String getBookableResourceId() {
		return planning.getBookableResource().getId();
	}

	@Override
	public int hashCode() {
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
		hashCodeBuilder.appendSuper(super.hashCode());
		hashCodeBuilder.append(getReference());
		hashCodeBuilder.append(getLabel());
		hashCodeBuilder.append(getBookableResourceId());
		return hashCodeBuilder.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		PlanningResponse other = (PlanningResponse) obj;
		EqualsBuilder equalsBuilder = new EqualsBuilder();
		equalsBuilder.appendSuper(super.equals(other));
		equalsBuilder.append(getReference(), other.getReference());
		equalsBuilder.append(getLabel(), other.getLabel());
		equalsBuilder.append(getBookableResourceId(), other.getBookableResourceId());
		return equalsBuilder.isEquals();
	}

}
