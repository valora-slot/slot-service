package dev.valora.slotservice.response;

import java.time.ZonedDateTime;
import java.util.UUID;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

import dev.valora.slotservice.model.PlanningTemplate;

public class PlanningTemplateResponse
		extends ResourceSupport {

	private PlanningTemplate planningTemplate;

	public PlanningTemplateResponse(PlanningTemplate planningTemplate) {
		this.planningTemplate = planningTemplate;
	}

	@JsonProperty("id")
	public UUID getReference() {
		return planningTemplate.getId();
	}

	public ZonedDateTime getStartDateTime() {
		return planningTemplate.getStartDateTime();
	}

	public ZonedDateTime getEndDateTime() {
		return planningTemplate.getEndDateTime();
	}

	public UUID getPlanningId() {
		return planningTemplate.getPlanning().getId();
	}

	@Override
	public int hashCode() {
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
		hashCodeBuilder.appendSuper(super.hashCode());
		hashCodeBuilder.append(getReference());
		hashCodeBuilder.append(getStartDateTime());
		hashCodeBuilder.append(getEndDateTime());
		hashCodeBuilder.append(getPlanningId());
		return hashCodeBuilder.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		PlanningTemplateResponse other = (PlanningTemplateResponse) obj;
		EqualsBuilder equalsBuilder = new EqualsBuilder();
		equalsBuilder.appendSuper(super.equals(other));
		equalsBuilder.append(getReference(), other.getReference());
		equalsBuilder.append(getStartDateTime(), other.getStartDateTime());
		equalsBuilder.append(getEndDateTime(), other.getEndDateTime());
		equalsBuilder.append(getPlanningId(), other.getPlanningId());
		return equalsBuilder.isEquals();
	}

}
