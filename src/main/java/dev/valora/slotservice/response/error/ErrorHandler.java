package dev.valora.slotservice.response.error;

import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.hateoas.VndErrors.VndError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
@RequestMapping(produces = "application/vnd.error+json")
public class ErrorHandler {

	private static final Logger LOG = LoggerFactory.getLogger(ErrorHandler.class);

	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity<VndError> entityNotFoundExceptionHandler(final EntityNotFoundException e) {
		return info(e, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public ResponseEntity<VndError> httpRequestMethodNotSupportedExceptionHandler(final HttpRequestMethodNotSupportedException e) {
		return info(e, HttpStatus.METHOD_NOT_ALLOWED);
	}

	@ExceptionHandler({ IllegalArgumentException.class, IllegalStateException.class, UnsatisfiedServletRequestParameterException.class,
			UnsatisfiedServletRequestParameterException.class })
	public ResponseEntity<VndError> badRequestExceptionHandler(final Exception e) {
		return info(e, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<VndError> methodArgumentTypeMismatchExceptionHandler(final MethodArgumentTypeMismatchException e) {
		return info(e, HttpStatus.BAD_REQUEST, "Wrong argument type");
	}

	@ExceptionHandler({ MethodArgumentNotValidException.class, BindException.class, PropertyReferenceException.class })
	public ResponseEntity<VndError> invalidRequestExceptionHandler(final Exception e) {
		return info(e, HttpStatus.BAD_REQUEST, "Invalid request");
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<VndError> httpMessageNotReadableExceptionHandler(final HttpMessageNotReadableException e) {
		return info(e, HttpStatus.BAD_REQUEST, "Malformed json request");
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<VndError> exceptionHandler(final Exception e) {
		final String logRef = UUID.randomUUID().toString();
		LOG.error("{} :", logRef, e);
		return new ResponseEntity<>(new VndError(logRef, "Internal server error"), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private ResponseEntity<VndError> info(final Exception exception, final HttpStatus httpStatus) {
		final String message = Optional.ofNullable(exception.getMessage()).orElse(exception.getClass().getSimpleName());
		return info(exception, httpStatus, message);
	}

	private ResponseEntity<VndError> info(final Exception exception, final HttpStatus httpStatus, final String message) {
		final String logRef = UUID.randomUUID().toString();
		LOG.info("{} : {}", logRef, message);
		LOG.debug("{} :", logRef, exception);
		return new ResponseEntity<>(new VndError(logRef, message), httpStatus);
	}

}
