package dev.valora.slotservice.response;

import java.time.ZonedDateTime;
import java.util.UUID;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

import dev.valora.slotservice.model.Slot;

public class SlotResponse
		extends ResourceSupport {

	private Slot slot;

	public SlotResponse(Slot slot) {
		this.slot = slot;
	}

	@JsonProperty("id")
	public String getReference() {
		return slot.getId();
	}

	public ZonedDateTime getStartDateTime() {
		return slot.getStartDateTime();
	}

	public ZonedDateTime getEndDateTime() {
		return slot.getEndDateTime();
	}

	public Integer getNumberOfReservations() {
		if (slot.getReservations() == null) {
			return 0;
		}
		return slot.getReservations().size();
	}

	public Integer getMaxCapacity() {
		return slot.getMaxCapacity();
	}

	public UUID getPlanningId() {
		return slot.getPlanning().getId();
	}

	public UUID getSlotTemplateId() {
		if (slot.getSlotTemplate() == null) {
			return null;
		}
		return slot.getSlotTemplate().getId();
	}

	@Override
	public int hashCode() {
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
		hashCodeBuilder.appendSuper(super.hashCode());
		hashCodeBuilder.append(getReference());
		hashCodeBuilder.append(getStartDateTime());
		hashCodeBuilder.append(getEndDateTime());
		hashCodeBuilder.append(getNumberOfReservations());
		hashCodeBuilder.append(getMaxCapacity());
		hashCodeBuilder.append(getPlanningId());
		hashCodeBuilder.append(getSlotTemplateId());
		return hashCodeBuilder.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		SlotResponse other = (SlotResponse) obj;
		EqualsBuilder equalsBuilder = new EqualsBuilder();
		equalsBuilder.appendSuper(super.equals(other));
		equalsBuilder.append(getReference(), other.getReference());
		equalsBuilder.append(getStartDateTime(), other.getStartDateTime());
		equalsBuilder.append(getEndDateTime(), other.getEndDateTime());
		equalsBuilder.append(getNumberOfReservations(), other.getNumberOfReservations());
		equalsBuilder.append(getMaxCapacity(), other.getMaxCapacity());
		equalsBuilder.append(getPlanningId(), other.getPlanningId());
		equalsBuilder.append(getSlotTemplateId(), other.getSlotTemplateId());
		return equalsBuilder.isEquals();
	}

}
