package dev.valora.slotservice.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.model.PlanningTemplate;
import dev.valora.slotservice.model.Slot;
import dev.valora.slotservice.model.SlotTemplate;
import dev.valora.slotservice.repository.PlanningRepository;
import dev.valora.slotservice.repository.PlanningTemplateRepository;
import dev.valora.slotservice.repository.SlotRepository;
import dev.valora.slotservice.repository.SlotTemplateRepository;
import dev.valora.slotservice.request.PlanningTemplateSearchCriteriaRequest;
import dev.valora.slotservice.request.SlotCreateRequest;
import dev.valora.slotservice.request.SlotSearchCriteriaRequest;

public class SlotService {

	private static final Logger LOG = LoggerFactory.getLogger(SlotService.class);

	private static final int UUID_LENGTH = 36;
	private static final int DATE_TIME_LENGTH = 8;

	private SlotRepository slotRepository;
	private PlanningRepository planningRepository;
	private PlanningTemplateRepository planningTemplateRepository;
	private SlotTemplateRepository slotTemplateRepository;
	private DateTimeFormatter idDateFormatter;

	public SlotService(SlotRepository slotRepository, PlanningRepository planningRepository, PlanningTemplateRepository planningTemplateRepository,
			SlotTemplateRepository slotTemplateRepository) {
		this.slotRepository = slotRepository;
		this.planningRepository = planningRepository;
		this.planningTemplateRepository = planningTemplateRepository;
		this.slotTemplateRepository = slotTemplateRepository;
		idDateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
	}

	public Iterable<Slot> findAll(SlotSearchCriteriaRequest slotSearchCriteriaRequest) {
		Collection<Slot> realSlots = slotRepository.findAll(slotSearchCriteriaRequest);

		PlanningTemplateSearchCriteriaRequest planningTemplateSearchCriteriaRequest = new PlanningTemplateSearchCriteriaRequest();
		planningTemplateSearchCriteriaRequest.setPlanningId(slotSearchCriteriaRequest.getPlanningId());
		planningTemplateSearchCriteriaRequest.setBookableResourceId(slotSearchCriteriaRequest.getBookableResourceId());
		planningTemplateSearchCriteriaRequest.setActiveDateTimeFrom(slotSearchCriteriaRequest.getStartDateTimeFrom());
		planningTemplateSearchCriteriaRequest.setActiveDateTimeTo(slotSearchCriteriaRequest.getStartDateTimeTo());
		List<PlanningTemplate> planningTemplates = planningTemplateRepository.findAll(planningTemplateSearchCriteriaRequest);

		Collection<Slot> virtualSlots = findVirtualSlots(planningTemplates, realSlots, slotSearchCriteriaRequest.getStartDateTimeFrom(),
				slotSearchCriteriaRequest.getStartDateTimeTo());

		Collection<Slot> slotsWithoutDeletedSlotTemplates = realSlots.stream().filter(slot -> slot.getMaxCapacity() != 0)
				.collect(Collectors.toList());
		slotsWithoutDeletedSlotTemplates.addAll(virtualSlots);
		return slotsWithoutDeletedSlotTemplates;
	}

	public Slot findById(String id) {
		return slotRepository.findById(id).orElseGet(() -> {
			if (isIdFromSlotTemplate(id)) {
				try {
					UUID slotTemplateId = getSlotTemplateIdFromId(id);
					LocalDate localDate = getStartDateFromId(id);

					return slotTemplateRepository.findById(slotTemplateId).map(slotTemplate -> {
						ZoneId timeZone = slotTemplate.getPlanningTemplate().getPlanning().getBookableResource().getTimeZone();
						ZonedDateTime startDateTime = localDate.atTime(slotTemplate.getStartTime()).atZone(timeZone);
						matchSlotTemplateDates(slotTemplate, startDateTime);

						Slot slotFromTemplate = new Slot();
						slotFromTemplate.setPlanning(slotTemplate.getPlanningTemplate().getPlanning());
						slotFromTemplate.setId(id);
						slotFromTemplate.setMaxCapacity(slotTemplate.getMaxCapacity());
						slotFromTemplate.setStartDateTime(startDateTime);
						slotFromTemplate.setEndDateTime(localDate.atTime(slotTemplate.getEndTime()).atZone(timeZone));
						return slotRepository.save(slotFromTemplate);
					}).orElseThrow(EntityNotFoundException::new);
				} catch (EntityNotFoundException | DateTimeParseException | IllegalArgumentException e) {
					LOG.debug("", e);
					throw new EntityNotFoundException(String.format("Cannot find slot for id: %s", id));
				}
			} else {
				throw new EntityNotFoundException(String.format("Cannot find slot for id: %s", id));
			}
		});
	}

	@Transactional
	public Slot create(SlotCreateRequest slotCreateRequest) {
		Planning planning = planningRepository.findById(slotCreateRequest.getPlanningId())
				.orElseThrow(() -> new IllegalArgumentException(String.format("Cannot find planning for id: %s", slotCreateRequest.getPlanningId())));

		Slot slot = new Slot();
		slot.setStartDateTime(slotCreateRequest.getStartDateTime());
		slot.setEndDateTime(slotCreateRequest.getEndDateTime());
		slot.setMaxCapacity(slotCreateRequest.getMaxCapacity());
		slot.setPlanning(planning);
		return slotRepository.save(slot);
	}

	@Transactional
	public void delete(String id) {
		Optional<Slot> slot = slotRepository.findById(id);
		if (slot.isPresent()) {
			Slot slotToDelete = slot.get();
			if (!slotToDelete.getReservations().isEmpty()) {
				throw new IllegalArgumentException("Cannot delete slot with reservations");
			}
			slotRepository.delete(slotToDelete);
		} else if (isIdFromSlotTemplate(id)) {
			try {
				UUID slotTemplateId = getSlotTemplateIdFromId(id);
				LocalDate localDate = getStartDateFromId(id);

				slotTemplateRepository.findById(slotTemplateId).ifPresent(slotTemplate -> {
					Slot slotTemplateToDelete = new Slot();
					slotTemplateToDelete.setSlotTemplate(slotTemplate);
					slotTemplateToDelete.setPlanning(slotTemplate.getPlanningTemplate().getPlanning());
					slotTemplateToDelete.setId(id);
					slotTemplateToDelete.setMaxCapacity(0);
					ZoneId timeZone = slotTemplate.getPlanningTemplate().getPlanning().getBookableResource().getTimeZone();
					slotTemplateToDelete.setStartDateTime(localDate.atTime(slotTemplate.getStartTime()).atZone(timeZone));
					slotTemplateToDelete.setEndDateTime(localDate.atTime(slotTemplate.getEndTime()).atZone(timeZone));
					slotRepository.save(slotTemplateToDelete);
				});
			} catch (IllegalArgumentException | DateTimeParseException e) {
				LOG.debug("", e);
			}
		}
	}

	private boolean isIdFromSlotTemplate(String id) {
		return id.length() == UUID_LENGTH + DATE_TIME_LENGTH;
	}

	private String getIdFromSlotTemplateIdAndSlotDate(UUID slotTemplateId, LocalDate slotDate) {
		return slotTemplateId.toString() + idDateFormatter.format(slotDate);
	}

	private UUID getSlotTemplateIdFromId(String id) {
		return UUID.fromString(id.substring(0, UUID_LENGTH));
	}

	private LocalDate getStartDateFromId(String id) {
		return LocalDate.parse(id.substring(UUID_LENGTH, UUID_LENGTH + DATE_TIME_LENGTH), idDateFormatter);
	}

	private Collection<Slot> findVirtualSlots(List<PlanningTemplate> planningTemplates, Collection<Slot> realSlots, ZonedDateTime searchDateTimeFrom,
			ZonedDateTime searchDateTimeTo) {
		Collection<Slot> virtualSlots = new ArrayList<>();
		if (!planningTemplates.isEmpty()) {
			ZoneId bookableResourceTimeZone = planningTemplates.get(0).getPlanning().getBookableResource().getTimeZone();

			planningTemplates.forEach(planningTemplate -> {
				ZonedDateTime startSearch;
				if (planningTemplate.getStartDateTime().isBefore(searchDateTimeFrom)) {
					startSearch = searchDateTimeFrom;
				} else {
					startSearch = planningTemplate.getStartDateTime();
				}

				ZonedDateTime endSearch;
				if (planningTemplate.getEndDateTime() == null || planningTemplate.getEndDateTime().isAfter(searchDateTimeTo)) {
					endSearch = searchDateTimeTo;
				} else {
					endSearch = planningTemplate.getEndDateTime();
				}

				addVirtualSlotsFromPlanningTemplate(virtualSlots, planningTemplate, realSlots, bookableResourceTimeZone, startSearch, endSearch);
			});
		}
		return virtualSlots;
	}

	private void addVirtualSlotsFromPlanningTemplate(Collection<Slot> virtualSlots, PlanningTemplate planningTemplate, Collection<Slot> realSlots,
			ZoneId bookableResourceTimeZone, ZonedDateTime startSearch, ZonedDateTime endSearch) {
		planningTemplate.getSlotTemplates().forEach(slotTemplate -> {
			if (slotTemplate.getMaxCapacity() != 0) {
				ZonedDateTime slotStartDateTime = startSearch.with(TemporalAdjusters.nextOrSame(slotTemplate.getDayOfWeek()))
						.withHour(slotTemplate.getStartTime().getHour()).withMinute(slotTemplate.getStartTime().getMinute())
						.withSecond(slotTemplate.getStartTime().getSecond()).withNano(0).withZoneSameLocal(bookableResourceTimeZone);
				if (slotStartDateTime.isBefore(startSearch)) {
					slotStartDateTime = slotStartDateTime.plusWeeks(1);
				}
				ZonedDateTime slotEndDateTime = slotStartDateTime.withHour(slotTemplate.getEndTime().getHour())
						.withMinute(slotTemplate.getEndTime().getMinute()).withSecond(slotTemplate.getEndTime().getSecond()).withNano(0);

				while (slotStartDateTime.isBefore(endSearch)) {
					Slot slotFromSlotTemplate = new Slot();
					slotFromSlotTemplate.setId(getIdFromSlotTemplateIdAndSlotDate(slotTemplate.getId(), slotStartDateTime.toLocalDate()));
					slotFromSlotTemplate.setStartDateTime(slotStartDateTime);
					slotFromSlotTemplate.setEndDateTime(slotEndDateTime);
					slotFromSlotTemplate.setSlotTemplate(slotTemplate);
					slotFromSlotTemplate.setPlanning(planningTemplate.getPlanning());
					slotFromSlotTemplate.setMaxCapacity(slotTemplate.getMaxCapacity());
					if (!realSlots.contains(slotFromSlotTemplate)) {
						virtualSlots.add(slotFromSlotTemplate);
					}

					slotStartDateTime = slotStartDateTime.plusWeeks(1);
					slotEndDateTime = slotEndDateTime.plusWeeks(1);
				}
			}
		});
	}

	private void matchSlotTemplateDates(SlotTemplate slotTemplate, ZonedDateTime slotIdStartDateTime) {
		ZonedDateTime planningTemplateStartDateTime = slotTemplate.getPlanningTemplate().getStartDateTime();
		ZonedDateTime planningTemplateEndDateTime = slotTemplate.getPlanningTemplate().getEndDateTime();
		if (slotTemplate.getDayOfWeek() != slotIdStartDateTime.getDayOfWeek() || slotIdStartDateTime.isBefore(planningTemplateStartDateTime)
				|| planningTemplateEndDateTime != null && slotIdStartDateTime.isAfter(planningTemplateEndDateTime)) {
			throw new EntityNotFoundException();
		}
	}

}
