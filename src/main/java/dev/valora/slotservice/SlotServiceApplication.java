package dev.valora.slotservice;

import java.time.ZonedDateTime;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.format.Formatter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import dev.valora.commons.springhateoasresourcesassembler.assembler.ResourcesAssembler;
import dev.valora.commons.springsecurityjpaauditing.SpringSecurityJpaAuditingConfiguration;
import dev.valora.slotservice.model.BookableResource;
import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.model.PlanningTemplate;
import dev.valora.slotservice.model.Reservation;
import dev.valora.slotservice.model.Slot;
import dev.valora.slotservice.model.SlotTemplate;
import dev.valora.slotservice.repository.PlanningRepository;
import dev.valora.slotservice.repository.PlanningTemplateRepository;
import dev.valora.slotservice.repository.SlotRepository;
import dev.valora.slotservice.repository.SlotTemplateRepository;
import dev.valora.slotservice.request.formatter.ZonedDateTimeFormatter;
import dev.valora.slotservice.response.BookableResourceResponse;
import dev.valora.slotservice.response.PlanningResponse;
import dev.valora.slotservice.response.PlanningTemplateResponse;
import dev.valora.slotservice.response.ReservationResponse;
import dev.valora.slotservice.response.SlotResponse;
import dev.valora.slotservice.response.SlotTemplateResponse;
import dev.valora.slotservice.response.assembler.BookableResourceResponseAssembler;
import dev.valora.slotservice.response.assembler.PlanningResponseAssembler;
import dev.valora.slotservice.response.assembler.PlanningTemplateResponseAssembler;
import dev.valora.slotservice.response.assembler.ReservationResponseAssembler;
import dev.valora.slotservice.response.assembler.SlotResponseAssembler;
import dev.valora.slotservice.response.assembler.SlotTemplateResponseAssembler;
import dev.valora.slotservice.service.SlotService;

@SpringBootApplication
@Import(SpringSecurityJpaAuditingConfiguration.class)
public class SlotServiceApplication
		extends WebSecurityConfigurerAdapter {

	@Value("${default.time.zone}")
	private String defaultTimeZone;

	@Value("${cors.allowed.origins}")
	private String[] corsAllowedOrigins;

	public static void main(String[] args) {
		SpringApplication.run(SlotServiceApplication.class, args);
	}

	@PostConstruct
	public void started() {
		TimeZone.setDefault(TimeZone.getTimeZone(defaultTimeZone));
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins(corsAllowedOrigins).allowedMethods("GET", "POST", "OPTIONS", "PUT", "PATCH", "DELETE")
						.allowedHeaders("*").exposedHeaders("Location");
			}
		};
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.apply(new SecurityAdapter());
	}

	public static class SecurityAdapter
			extends AbstractHttpConfigurer<SecurityAdapter, HttpSecurity> {

		@Override
		public void init(HttpSecurity http) throws Exception {
			http.csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests().anyRequest()
					.permitAll();
		}

	}

	@Bean
	public Formatter<ZonedDateTime> zonedDateTimeFormatter() {
		return new ZonedDateTimeFormatter();
	}

	@Bean
	public SlotService slotService(SlotRepository slotRepository, PlanningRepository planningRepository,
			PlanningTemplateRepository planningTemplateRepository, SlotTemplateRepository slotTemplateRepository) {
		return new SlotService(slotRepository, planningRepository, planningTemplateRepository, slotTemplateRepository);
	}

	@Bean
	public ResourcesAssembler<BookableResource, BookableResourceResponse> bookableResourceResponseAssembler(
			PagedResourcesAssembler<BookableResource> pagedResourcesAssembler) {
		return new ResourcesAssembler<>(pagedResourcesAssembler, new BookableResourceResponseAssembler(), BookableResourceResponse.class);
	}

	@Bean
	public ResourcesAssembler<Planning, PlanningResponse> planningResponseAssembler(PagedResourcesAssembler<Planning> pagedResourcesAssembler) {
		return new ResourcesAssembler<>(pagedResourcesAssembler, new PlanningResponseAssembler(), PlanningResponse.class);
	}

	@Bean
	public ResourcesAssembler<PlanningTemplate, PlanningTemplateResponse> planningTemplateResponseAssembler(
			PagedResourcesAssembler<PlanningTemplate> pagedResourcesAssembler) {
		return new ResourcesAssembler<>(pagedResourcesAssembler, new PlanningTemplateResponseAssembler(), PlanningTemplateResponse.class);
	}

	@Bean
	public ResourcesAssembler<SlotTemplate, SlotTemplateResponse> slotTemplateResponseAssembler(
			PagedResourcesAssembler<SlotTemplate> pagedResourcesAssembler) {
		return new ResourcesAssembler<>(pagedResourcesAssembler, new SlotTemplateResponseAssembler(), SlotTemplateResponse.class);
	}

	@Bean
	public ResourcesAssembler<Slot, SlotResponse> slotResponseAssembler(PagedResourcesAssembler<Slot> pagedResourcesAssembler) {
		return new ResourcesAssembler<>(pagedResourcesAssembler, new SlotResponseAssembler(), SlotResponse.class);
	}

	@Bean
	public ResourcesAssembler<Reservation, ReservationResponse> reservationResponseAssembler(
			PagedResourcesAssembler<Reservation> pagedResourcesAssembler) {
		return new ResourcesAssembler<>(pagedResourcesAssembler, new ReservationResponseAssembler(), ReservationResponse.class);
	}

}
