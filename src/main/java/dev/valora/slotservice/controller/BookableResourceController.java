package dev.valora.slotservice.controller;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.valora.commons.springhateoasresourcesassembler.assembler.ResourcesAssembler;
import dev.valora.slotservice.model.BookableResource;
import dev.valora.slotservice.repository.BookableResourceRepository;
import dev.valora.slotservice.request.BookableResourceSaveRequest;
import dev.valora.slotservice.response.BookableResourceResponse;

@RestController
@RequestMapping(path = "/bookable-resources", produces = MediaTypes.HAL_JSON_VALUE)
public class BookableResourceController {

	private BookableResourceRepository bookableResourceRepository;
	private ResourcesAssembler<BookableResource, BookableResourceResponse> responseAssembler;

	public BookableResourceController(BookableResourceRepository bookableResourceRepository,
			ResourcesAssembler<BookableResource, BookableResourceResponse> responseAssembler) {
		this.bookableResourceRepository = bookableResourceRepository;
		this.responseAssembler = responseAssembler;
	}

	@GetMapping
	public Resources<BookableResourceResponse> findAll() {
		Iterable<BookableResource> bookableResources = bookableResourceRepository.findAll();
		return responseAssembler.toResources(bookableResources);
	}

	@GetMapping("/{id}")
	public BookableResourceResponse findById(@PathVariable String id) {
		BookableResource bookableResource = bookableResourceRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find bookable resource for id: %s", id)));
		return responseAssembler.toResource(bookableResource);
	}

	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> save(@PathVariable String id, @RequestBody @Valid BookableResourceSaveRequest bookableResourceSaveRequest) {
		Optional<BookableResource> optional = bookableResourceRepository.findById(id);

		BookableResource bookableResource = optional.orElse(new BookableResource());
		bookableResource.setId(id);
		bookableResource.setLabel(bookableResourceSaveRequest.getLabel());
		bookableResource.setTimeZone(bookableResourceSaveRequest.getTimeZone());
		bookableResourceRepository.save(bookableResource);

		if (optional.isPresent()) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.created(null).build();
	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> delete(@PathVariable String id) {
		bookableResourceRepository.findById(id).ifPresent(bookableResource -> bookableResourceRepository.delete(bookableResource));
		return ResponseEntity.noContent().build();
	}

}
