package dev.valora.slotservice.controller;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

import java.net.URI;
import java.util.UUID;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.valora.commons.springhateoasresourcesassembler.assembler.ResourcesAssembler;
import dev.valora.slotservice.model.Reservation;
import dev.valora.slotservice.model.Slot;
import dev.valora.slotservice.repository.ReservationRepository;
import dev.valora.slotservice.request.ReservationCreateRequest;
import dev.valora.slotservice.request.ReservationPatchRequest;
import dev.valora.slotservice.request.ReservationSaveRequest;
import dev.valora.slotservice.request.ReservationSearchCriteriaRequest;
import dev.valora.slotservice.response.ReservationResponse;
import dev.valora.slotservice.service.SlotService;

@RestController
@RequestMapping(path = "/reservations", produces = MediaTypes.HAL_JSON_VALUE)
public class ReservationController {

	private static final String NOT_FOUND_MESSAGE = "Cannot find slot for id: %s";
	private static final String FULL_SLOT_MESSAGE = "Full slot";

	private ReservationRepository reservationRepository;
	private SlotService slotService;
	private ResourcesAssembler<Reservation, ReservationResponse> responseAssembler;

	public ReservationController(ReservationRepository reservationRepository, SlotService slotService,
			ResourcesAssembler<Reservation, ReservationResponse> responseAssembler) {
		this.reservationRepository = reservationRepository;
		this.slotService = slotService;
		this.responseAssembler = responseAssembler;
	}

	@GetMapping
	public Resources<ReservationResponse> findAll(@Valid ReservationSearchCriteriaRequest reservationSearchCriteriaRequest) {
		Iterable<Reservation> reservations = reservationRepository.findAll(reservationSearchCriteriaRequest);
		return responseAssembler.toResources(reservations);
	}

	@GetMapping("/{id}")
	public ReservationResponse findById(@PathVariable UUID id) {
		Reservation reservation = reservationRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find reservation for id: %s", id)));
		return responseAssembler.toResource(reservation);
	}

	@PostMapping
	@Transactional
	public ResponseEntity<Void> create(@RequestBody @Valid ReservationCreateRequest reservationCreateRequest) {
		Slot slot;
		try {
			slot = slotService.findById(reservationCreateRequest.getSlotId());
		} catch (EntityNotFoundException e) {
			throw new IllegalArgumentException(String.format(NOT_FOUND_MESSAGE, reservationCreateRequest.getSlotId()));
		}

		if (slot.getReservations() != null && slot.getReservations().size() >= slot.getMaxCapacity()) {
			throw new IllegalArgumentException(FULL_SLOT_MESSAGE);
		}

		Reservation reservation = new Reservation();
		reservation.setSlot(slot);
		reservation.setOwner(reservationCreateRequest.getOwner());
		reservation.setEndDateTime(reservationCreateRequest.getEndDateTime());
		reservation = reservationRepository.save(reservation);

		final URI uri = fromCurrentRequest().path("/{id}").buildAndExpand(reservation.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> save(@PathVariable UUID id, @RequestBody @Valid ReservationSaveRequest reservationSaveRequest) {
		Reservation reservation = reservationRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find reservation for id: %s", id)));

		Slot slot;
		try {
			slot = slotService.findById(reservationSaveRequest.getSlotId());
		} catch (EntityNotFoundException e) {
			throw new IllegalArgumentException(String.format(NOT_FOUND_MESSAGE, reservationSaveRequest.getSlotId()));
		}

		if (slot.getReservations() != null && slot.getReservations().size() >= slot.getMaxCapacity()) {
			throw new IllegalArgumentException(FULL_SLOT_MESSAGE);
		}

		reservation.setSlot(slot);
		reservation.setOwner(reservationSaveRequest.getOwner());
		reservation.setEndDateTime(reservationSaveRequest.getEndDateTime());
		reservationRepository.save(reservation);

		return ResponseEntity.noContent().build();
	}

	@PatchMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> update(@PathVariable UUID id, @RequestBody @Valid ReservationPatchRequest reservationPatchRequest) {
		Reservation reservation = reservationRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find reservation for id: %s", id)));

		if (reservationPatchRequest.getSlotId() != null && !reservation.getSlot().getId().equals(reservationPatchRequest.getSlotId())) {
			Slot slot;
			try {
				slot = slotService.findById(reservationPatchRequest.getSlotId());
			} catch (EntityNotFoundException e) {
				throw new IllegalArgumentException(String.format(NOT_FOUND_MESSAGE, reservationPatchRequest.getSlotId()));
			}

			if (slot.getReservations() != null && slot.getReservations().size() >= slot.getMaxCapacity()) {
				throw new IllegalArgumentException(FULL_SLOT_MESSAGE);
			}
			reservation.setSlot(slot);
		}

		if (reservationPatchRequest.getOwner() != null) {
			reservation.setOwner(reservationPatchRequest.getOwner());
		}

		if (reservationPatchRequest.getEndDateTime() != null) {
			reservation.setEndDateTime(reservationPatchRequest.getEndDateTime());
		}

		reservationRepository.save(reservation);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> delete(@PathVariable UUID id) {
		reservationRepository.findById(id).ifPresent(reservation -> reservationRepository.delete(reservation));
		return ResponseEntity.noContent().build();
	}

}
