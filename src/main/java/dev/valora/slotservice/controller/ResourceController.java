package dev.valora.slotservice.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaTypes.HAL_JSON_VALUE)
public class ResourceController {

	@GetMapping("/")
	public ResourceSupport findAll() {
		ResourceSupport resources = new ResourceSupport();
		resources.add(linkTo(BookableResourceController.class).withRel("bookableResources"));
		resources.add(linkTo(PlanningController.class).withRel("plannings"));
		resources.add(linkTo(PlanningTemplateController.class).withRel("planningTemplates"));
		resources.add(linkTo(ReservationController.class).withRel("reservations"));
		resources.add(linkTo(SlotController.class).withRel("slots"));
		resources.add(linkTo(SlotTemplateController.class).withRel("slotTemplates"));
		return resources;
	}

}
