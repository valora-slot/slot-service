package dev.valora.slotservice.controller;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.valora.commons.springhateoasresourcesassembler.assembler.ResourcesAssembler;
import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.model.Slot;
import dev.valora.slotservice.repository.PlanningRepository;
import dev.valora.slotservice.repository.SlotRepository;
import dev.valora.slotservice.request.SlotCreateRequest;
import dev.valora.slotservice.request.SlotSaveRequest;
import dev.valora.slotservice.request.SlotSearchCriteriaRequest;
import dev.valora.slotservice.response.SlotResponse;
import dev.valora.slotservice.service.SlotService;

@RestController
@RequestMapping(path = "/slots", produces = MediaTypes.HAL_JSON_VALUE)
public class SlotController {

	private SlotRepository slotRepository;
	private PlanningRepository planningRepository;
	private ResourcesAssembler<Slot, SlotResponse> responseAssembler;
	private SlotService slotService;

	public SlotController(SlotRepository slotRepository, PlanningRepository planningRepository,
			ResourcesAssembler<Slot, SlotResponse> responseAssembler, SlotService slotService) {
		this.slotRepository = slotRepository;
		this.planningRepository = planningRepository;
		this.responseAssembler = responseAssembler;
		this.slotService = slotService;
	}

	@GetMapping
	public Resources<SlotResponse> findAll(@Valid SlotSearchCriteriaRequest slotSearchCriteriaRequest) {
		Iterable<Slot> slots = slotService.findAll(slotSearchCriteriaRequest);
		return responseAssembler.toResources(slots);
	}

	@GetMapping("/{id}")
	public SlotResponse findById(@PathVariable String id) {
		return responseAssembler.toResource(slotService.findById(id));
	}

	@PostMapping
	public ResponseEntity<Void> create(@RequestBody @Valid SlotCreateRequest slotCreateRequest) {
		Slot slot = slotService.create(slotCreateRequest);
		final URI uri = fromCurrentRequest().path("/{id}").buildAndExpand(slot.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> save(@PathVariable String id, @RequestBody @Valid SlotSaveRequest slotSaveRequest) {
		Optional<Slot> optional = slotRepository.findById(id);

		Planning planning = planningRepository.findById(slotSaveRequest.getPlanningId())
				.orElseThrow(() -> new IllegalArgumentException(String.format("Cannot find planning for id: %s", slotSaveRequest.getPlanningId())));

		Slot slot = optional.orElse(new Slot());
		slot.setId(id);
		slot.setStartDateTime(slotSaveRequest.getStartDateTime());
		slot.setEndDateTime(slotSaveRequest.getEndDateTime());
		slot.setMaxCapacity(slotSaveRequest.getMaxCapacity());
		slot.setPlanning(planning);
		slot.setSlotTemplate(null);
		slotRepository.save(slot);

		if (optional.isPresent()) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.created(null).build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable String id) {
		slotService.delete(id);
		return ResponseEntity.noContent().build();
	}

}
