package dev.valora.slotservice.controller;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

import java.net.URI;
import java.util.UUID;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.valora.commons.springhateoasresourcesassembler.assembler.ResourcesAssembler;
import dev.valora.slotservice.model.BookableResource;
import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.repository.BookableResourceRepository;
import dev.valora.slotservice.repository.PlanningRepository;
import dev.valora.slotservice.request.PlanningCreateRequest;
import dev.valora.slotservice.request.PlanningSearchCriteriaRequest;
import dev.valora.slotservice.response.PlanningResponse;

@RestController
@RequestMapping(path = "/plannings", produces = MediaTypes.HAL_JSON_VALUE)
public class PlanningController {

	private PlanningRepository planningRepository;
	private BookableResourceRepository bookableResourceRepository;
	private ResourcesAssembler<Planning, PlanningResponse> responseAssembler;

	public PlanningController(PlanningRepository planningRepository, BookableResourceRepository bookableResourceRepository,
			ResourcesAssembler<Planning, PlanningResponse> responseAssembler) {
		this.planningRepository = planningRepository;
		this.bookableResourceRepository = bookableResourceRepository;
		this.responseAssembler = responseAssembler;
	}

	@GetMapping
	public Resources<PlanningResponse> findAll(@Valid PlanningSearchCriteriaRequest planningSearchCriteriaRequest) {
		Iterable<Planning> plannings = planningRepository.findAll(planningSearchCriteriaRequest);
		return responseAssembler.toResources(plannings);
	}

	@GetMapping("/{id}")
	public PlanningResponse findById(@PathVariable UUID id) {
		Planning planning = planningRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find planning for id: %s", id)));
		return responseAssembler.toResource(planning);
	}

	@PostMapping
	@Transactional
	public ResponseEntity<Void> create(@RequestBody @Valid PlanningCreateRequest planningCreateRequest) {
		BookableResource bookableResource = bookableResourceRepository.findById(planningCreateRequest.getBookableResourceId())
				.orElseThrow(() -> new IllegalArgumentException(
						String.format("Cannot find bookable resource for id: %s", planningCreateRequest.getBookableResourceId())));

		Planning planning = new Planning();
		planning.setLabel(planningCreateRequest.getLabel());
		planning.setBookableResource(bookableResource);
		planning = planningRepository.save(planning);

		final URI uri = fromCurrentRequest().path("/{id}").buildAndExpand(planning.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

}
