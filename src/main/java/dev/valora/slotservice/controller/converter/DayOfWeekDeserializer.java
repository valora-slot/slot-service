package dev.valora.slotservice.controller.converter;

import java.io.IOException;
import java.time.DayOfWeek;

import org.springframework.boot.jackson.JsonComponent;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

@JsonComponent
public class DayOfWeekDeserializer
		extends StdDeserializer<DayOfWeek> {

	private static final long serialVersionUID = -969363985277261426L;

	protected DayOfWeekDeserializer() {
		super(DayOfWeek.class);
	}

	@Override
	public DayOfWeek deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
		return DayOfWeek.of(jsonParser.getIntValue());
	}

}
