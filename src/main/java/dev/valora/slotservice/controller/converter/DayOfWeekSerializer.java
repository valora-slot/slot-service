package dev.valora.slotservice.controller.converter;

import java.io.IOException;
import java.time.DayOfWeek;

import org.springframework.boot.jackson.JsonComponent;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

@JsonComponent
public class DayOfWeekSerializer
		extends StdSerializer<DayOfWeek> {

	private static final long serialVersionUID = 445600088073738258L;

	protected DayOfWeekSerializer() {
		super(DayOfWeek.class);
	}

	@Override
	public void serialize(DayOfWeek value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
		jsonGenerator.writeNumber(value.getValue());
	}

}
