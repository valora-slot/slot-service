package dev.valora.slotservice.controller;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

import java.net.URI;
import java.util.UUID;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.valora.commons.springhateoasresourcesassembler.assembler.ResourcesAssembler;
import dev.valora.slotservice.model.PlanningTemplate;
import dev.valora.slotservice.model.SlotTemplate;
import dev.valora.slotservice.repository.PlanningTemplateRepository;
import dev.valora.slotservice.repository.SlotTemplateRepository;
import dev.valora.slotservice.request.SlotTemplateCreateRequest;
import dev.valora.slotservice.request.SlotTemplatePatchRequest;
import dev.valora.slotservice.request.SlotTemplateSearchCriteriaRequest;
import dev.valora.slotservice.response.SlotTemplateResponse;

@RestController
@RequestMapping(path = "/slot-templates", produces = MediaTypes.HAL_JSON_VALUE)
public class SlotTemplateController {

	private static final String NOT_FOUND_MESSAGE = "Cannot find slot template for id: %s";

	private SlotTemplateRepository slotTemplateRepository;
	private PlanningTemplateRepository planningTemplateRepository;
	private ResourcesAssembler<SlotTemplate, SlotTemplateResponse> responseAssembler;

	public SlotTemplateController(SlotTemplateRepository slotTemplateRepository, PlanningTemplateRepository planningTemplateRepository,
			ResourcesAssembler<SlotTemplate, SlotTemplateResponse> responseAssembler) {
		this.slotTemplateRepository = slotTemplateRepository;
		this.planningTemplateRepository = planningTemplateRepository;
		this.responseAssembler = responseAssembler;
	}

	@GetMapping
	public Resources<SlotTemplateResponse> findAll(@Valid SlotTemplateSearchCriteriaRequest slotTemplateSearchCriteriaRequest) {
		Iterable<SlotTemplate> slotTemplates = slotTemplateRepository.findAll(slotTemplateSearchCriteriaRequest);
		return responseAssembler.toResources(slotTemplates);
	}

	@GetMapping("/{id}")
	public SlotTemplateResponse findById(@PathVariable UUID id) {
		SlotTemplate slotTemplate = slotTemplateRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(String.format(NOT_FOUND_MESSAGE, id)));
		return responseAssembler.toResource(slotTemplate);
	}

	@PostMapping
	@Transactional
	public ResponseEntity<Void> create(@RequestBody @Valid SlotTemplateCreateRequest slotTemplateCreateRequest) {
		PlanningTemplate planningTemplate = planningTemplateRepository.findById(slotTemplateCreateRequest.getPlanningTemplateId())
				.orElseThrow(() -> new IllegalArgumentException(
						String.format("Cannot find planning template for id: %s", slotTemplateCreateRequest.getPlanningTemplateId())));

		SlotTemplate slotTemplate = new SlotTemplate();

		slotTemplate.setDayOfWeek(slotTemplateCreateRequest.getDayOfWeek());
		slotTemplate.setStartTime(slotTemplateCreateRequest.getStartTime());
		slotTemplate.setEndTime(slotTemplateCreateRequest.getEndTime());
		slotTemplate.setMaxCapacity(slotTemplateCreateRequest.getMaxCapacity());
		slotTemplate.setPlanningTemplate(planningTemplate);
		slotTemplate = slotTemplateRepository.save(slotTemplate);

		final URI uri = fromCurrentRequest().path("/{id}").buildAndExpand(slotTemplate.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@PatchMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> update(@PathVariable UUID id, @RequestBody @Valid SlotTemplatePatchRequest slotTemplatePatchRequest) {
		SlotTemplate slotTemplate = slotTemplateRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(String.format(NOT_FOUND_MESSAGE, id)));

		if (slotTemplatePatchRequest.getDayOfWeek() != null) {
			slotTemplate.setDayOfWeek(slotTemplatePatchRequest.getDayOfWeek());
		}
		if (slotTemplatePatchRequest.getStartTime() != null) {
			slotTemplate.setStartTime(slotTemplatePatchRequest.getStartTime());
		}
		if (slotTemplatePatchRequest.getEndTime() != null) {
			slotTemplate.setEndTime(slotTemplatePatchRequest.getEndTime());
		}
		if (slotTemplatePatchRequest.getMaxCapacity() != null) {
			slotTemplate.setMaxCapacity(slotTemplatePatchRequest.getMaxCapacity());
		}
		slotTemplateRepository.save(slotTemplate);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> delete(@PathVariable UUID id) {
		slotTemplateRepository.findById(id).ifPresent(slotTemplate -> slotTemplateRepository.delete(slotTemplate));
		return ResponseEntity.noContent().build();
	}

}
