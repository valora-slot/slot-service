package dev.valora.slotservice.controller;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

import java.net.URI;
import java.util.UUID;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.valora.commons.springhateoasresourcesassembler.assembler.ResourcesAssembler;
import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.model.PlanningTemplate;
import dev.valora.slotservice.repository.PlanningRepository;
import dev.valora.slotservice.repository.PlanningTemplateRepository;
import dev.valora.slotservice.request.PlanningTemplateCreateRequest;
import dev.valora.slotservice.request.PlanningTemplatePatchRequest;
import dev.valora.slotservice.request.PlanningTemplateSearchCriteriaRequest;
import dev.valora.slotservice.request.PlanningTemplateUpdateRequest;
import dev.valora.slotservice.response.PlanningTemplateResponse;

@RestController
@RequestMapping(path = "/planning-templates", produces = MediaTypes.HAL_JSON_VALUE)
public class PlanningTemplateController {

	private static final String NOT_FOUND_MESSAGE = "Cannot find planning template for id: %s";

	private PlanningTemplateRepository planningTemplateRepository;
	private PlanningRepository planningRepository;
	private ResourcesAssembler<PlanningTemplate, PlanningTemplateResponse> responseAssembler;

	public PlanningTemplateController(PlanningTemplateRepository planningTemplateRepository, PlanningRepository planningRepository,
			ResourcesAssembler<PlanningTemplate, PlanningTemplateResponse> responseAssembler) {
		this.planningTemplateRepository = planningTemplateRepository;
		this.planningRepository = planningRepository;
		this.responseAssembler = responseAssembler;
	}

	@GetMapping
	public Resources<PlanningTemplateResponse> findAll(@Valid PlanningTemplateSearchCriteriaRequest planningTemplateSearchCriteriaRequest,
			Pageable pageable) {
		Page<PlanningTemplate> planningTemplates = planningTemplateRepository.findAll(planningTemplateSearchCriteriaRequest, pageable);
		return responseAssembler.toResources(planningTemplates);
	}

	@GetMapping("/{id}")
	public PlanningTemplateResponse findById(@PathVariable UUID id) {
		PlanningTemplate planningTemplate = planningTemplateRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(String.format(NOT_FOUND_MESSAGE, id)));
		return responseAssembler.toResource(planningTemplate);
	}

	@PostMapping
	@Transactional
	public ResponseEntity<Void> create(@RequestBody @Valid PlanningTemplateCreateRequest planningTemplateCreateRequest) {
		Planning planning = planningRepository.findById(planningTemplateCreateRequest.getPlanningId()).orElseThrow(
				() -> new IllegalArgumentException(String.format("Cannot find planning for id: %s", planningTemplateCreateRequest.getPlanningId())));

		PlanningTemplate planningTemplate = new PlanningTemplate();
		planningTemplate.setStartDateTime(planningTemplateCreateRequest.getStartDateTime());
		planningTemplate.setEndDateTime(planningTemplateCreateRequest.getEndDateTime());
		planningTemplate.setPlanning(planning);
		planningTemplate = planningTemplateRepository.save(planningTemplate);

		final URI uri = fromCurrentRequest().path("/{id}").buildAndExpand(planningTemplate.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> save(@PathVariable UUID id, @RequestBody @Valid PlanningTemplateUpdateRequest planningTemplateUpdateRequest) {
		PlanningTemplate planningTemplate = planningTemplateRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(String.format(NOT_FOUND_MESSAGE, id)));

		planningTemplate.setStartDateTime(planningTemplateUpdateRequest.getStartDateTime());
		planningTemplate.setEndDateTime(planningTemplateUpdateRequest.getEndDateTime());
		if (!planningTemplateUpdateRequest.getPlanningId().equals(planningTemplate.getPlanning().getId())) {
			Planning planning = planningRepository.findById(planningTemplateUpdateRequest.getPlanningId())
					.orElseThrow(() -> new IllegalArgumentException(
							String.format("Cannot find planning for id: %s", planningTemplateUpdateRequest.getPlanningId())));
			planningTemplate.setPlanning(planning);
		}

		planningTemplateRepository.save(planningTemplate);
		return ResponseEntity.noContent().build();
	}

	@PatchMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> update(@PathVariable UUID id, @RequestBody @Valid PlanningTemplatePatchRequest planningTemplatePatchRequest) {
		PlanningTemplate planningTemplate = planningTemplateRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(String.format(NOT_FOUND_MESSAGE, id)));

		if (planningTemplatePatchRequest.getStartDateTime() != null) {
			planningTemplate.setStartDateTime(planningTemplatePatchRequest.getStartDateTime());
		}
		if (planningTemplatePatchRequest.getEndDateTime() != null) {
			planningTemplate.setEndDateTime(planningTemplatePatchRequest.getEndDateTime());
		}
		planningTemplateRepository.save(planningTemplate);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> delete(@PathVariable UUID id) {
		planningTemplateRepository.findById(id).ifPresent(planningTemplate -> planningTemplateRepository.delete(planningTemplate));
		return ResponseEntity.noContent().build();
	}

}
