package dev.valora.slotservice.functional.test;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import dev.valora.slotservice.model.BookableResource;
import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.model.PlanningTemplate;
import dev.valora.slotservice.model.Slot;
import dev.valora.slotservice.model.SlotTemplate;
import dev.valora.slotservice.repository.BookableResourceRepository;
import dev.valora.slotservice.repository.PlanningRepository;
import dev.valora.slotservice.repository.PlanningTemplateRepository;
import dev.valora.slotservice.repository.SlotRepository;
import dev.valora.slotservice.repository.SlotTemplateRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class SlotTemplateTests {

	private static final String BASE_CONTROLLER_URL = "/slot-templates";
	private static final UUID NOT_FOUND_SLOT_TEMPLATE_ID = UUID.fromString("44444444-0000-0000-0000-444444444444");
	private static final UUID NOT_FOUND_PLANNING_TEMPLATE_ID = UUID.fromString("44444444-0000-0000-0000-444444444444");
	private static final String BOOKABLE_RESOURCE_ID = "ref1";
	private static final String START_DATE_TIME_VALUE = "2019-08-29T21:18:25.652Z";
	private static ZonedDateTime START_DATE_TIME;
	private static final String END_DATE_TIME_VALUE = "2019-09-07T21:18:25.652Z";
	private static ZonedDateTime END_DATE_TIME;
	private static final String START_DATE_TIME2_VALUE = "2019-09-01T21:18:25.652Z";
	private static ZonedDateTime START_DATE_TIME2;
	private static final ZonedDateTime END_DATE_TIME2 = null;
	private static final String START_TIME_VALUE = "21:00:00";
	private static LocalTime START_TIME;
	private static final String END_TIME_VALUE = "21:30:35";
	private static LocalTime END_TIME;
	private static final int MAX_CAPACITY = 10;
	private static final String START_TIME2_VALUE = "12:30:00";
	private static LocalTime START_TIME2;
	private static final String END_TIME2_VALUE = "21:30:35";
	private static LocalTime END_TIME2;
	private static final int MAX_CAPACITY2 = 2;

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private BookableResourceRepository bookableResourceRepository;
	@Autowired
	private PlanningRepository planningRepository;
	@Autowired
	private PlanningTemplateRepository planningTemplateRepository;
	@Autowired
	private SlotTemplateRepository slotTemplateRepository;
	@Autowired
	private SlotRepository slotRepository;

	private BookableResource bookableResource;
	private Planning planning;
	private PlanningTemplate planningTemplate;
	private SlotTemplate slotTemplate;
	private Slot slot;
	private PlanningTemplate planningTemplate2;
	private SlotTemplate slotTemplate2;

	public SlotTemplateTests() {
		START_DATE_TIME = ZonedDateTime.parse(START_DATE_TIME_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		END_DATE_TIME = ZonedDateTime.parse(END_DATE_TIME_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		START_DATE_TIME2 = ZonedDateTime.parse(START_DATE_TIME2_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		START_TIME = LocalTime.parse(START_TIME_VALUE);
		END_TIME = LocalTime.parse(END_TIME_VALUE);
		START_TIME2 = LocalTime.parse(START_TIME2_VALUE);
		END_TIME2 = LocalTime.parse(END_TIME2_VALUE);

		bookableResource = new BookableResource();
		bookableResource.setId(BOOKABLE_RESOURCE_ID);
		bookableResource.setTimeZone(ZoneId.of("Europe/Paris"));

		planning = new Planning();
		planning.setBookableResource(bookableResource);

		planningTemplate = new PlanningTemplate();
		planningTemplate.setPlanning(planning);
		planningTemplate.setStartDateTime(START_DATE_TIME);
		planningTemplate.setEndDateTime(END_DATE_TIME);

		slotTemplate = new SlotTemplate();
		slotTemplate.setPlanningTemplate(planningTemplate);
		slotTemplate.setDayOfWeek(DayOfWeek.FRIDAY);
		slotTemplate.setMaxCapacity(MAX_CAPACITY);
		slotTemplate.setStartTime(START_TIME);
		slotTemplate.setEndTime(END_TIME);

		slot = new Slot();
		slot.setPlanning(planning);
		slot.setSlotTemplate(slotTemplate);
		slot.setStartDateTime(START_DATE_TIME);
		slot.setEndDateTime(END_DATE_TIME);
		slot.setMaxCapacity(MAX_CAPACITY);

		planningTemplate2 = new PlanningTemplate();
		planningTemplate2.setPlanning(planning);
		planningTemplate2.setStartDateTime(START_DATE_TIME2);
		planningTemplate2.setEndDateTime(END_DATE_TIME2);

		slotTemplate2 = new SlotTemplate();
		slotTemplate2.setPlanningTemplate(planningTemplate2);
		slotTemplate2.setDayOfWeek(DayOfWeek.MONDAY);
		slotTemplate2.setMaxCapacity(MAX_CAPACITY2);
		slotTemplate2.setStartTime(START_TIME2);
		slotTemplate2.setEndTime(END_TIME2);
	}

	@After
	public void tearDown() {
		slotRepository.deleteAll();
		slotTemplateRepository.deleteAll();
		planningTemplateRepository.deleteAll();
		planningRepository.deleteAll();
		bookableResourceRepository.deleteAll();
	}

	@Test
	public void findAllWithEmptyList() throws Exception {
		mockMvc.perform(get(BASE_CONTROLLER_URL)).andExpect(status().isOk()).andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList", hasSize(0)));
	}

	@Test
	public void findAllWithOneSlotTemplate() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		mockMvc.perform(get(BASE_CONTROLLER_URL)).andExpect(status().isOk()).andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList", hasSize(1)))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList[0].id").value(slotTemplate.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList[0].dayOfWeek").value(DayOfWeek.FRIDAY.getValue()))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList[0].startTime").value(START_TIME_VALUE))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList[0].endTime").value(END_TIME_VALUE))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList[0].maxCapacity").value(MAX_CAPACITY))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList[0].planningTemplateId").value(planningTemplate.getId().toString()));
	}

	@Test
	public void findAllByPlanningTemplateId() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);
		planningTemplateRepository.save(planningTemplate2);
		slotTemplateRepository.save(slotTemplate2);

		mockMvc.perform(get(BASE_CONTROLLER_URL).param("planningTemplateId", planningTemplate2.getId().toString())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8)).andExpect(jsonPath("$._embedded.slotTemplateResponseList", hasSize(1)))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList[0].id").value(slotTemplate2.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList[0].dayOfWeek").value(DayOfWeek.MONDAY.getValue()))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList[0].startTime").value(START_TIME2_VALUE))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList[0].endTime").value(END_TIME2_VALUE))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList[0].maxCapacity").value(MAX_CAPACITY2))
				.andExpect(jsonPath("$._embedded.slotTemplateResponseList[0].planningTemplateId").value(planningTemplate2.getId().toString()));
	}

	@Test
	public void findByIdNonExisting() throws Exception {
		mockMvc.perform(get(String.format("%s/%s", BASE_CONTROLLER_URL, NOT_FOUND_SLOT_TEMPLATE_ID))).andExpect(status().isNotFound());
	}

	@Test
	public void findByIdExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		mockMvc.perform(get(String.format("%s/%s", BASE_CONTROLLER_URL, slotTemplate.getId()))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8)).andExpect(jsonPath("$.id").value(slotTemplate.getId().toString()))
				.andExpect(jsonPath("$.dayOfWeek").value(DayOfWeek.FRIDAY.getValue())).andExpect(jsonPath("$.startTime").value(START_TIME_VALUE))
				.andExpect(jsonPath("$.endTime").value(END_TIME_VALUE)).andExpect(jsonPath("$.maxCapacity").value(MAX_CAPACITY))
				.andExpect(jsonPath("$.planningTemplateId").value(planningTemplate.getId().toString()));
	}

	@Test
	public void createWithNonExistingPlanningTemplate() throws Exception {
		String requestBody = String.format(
				"{\"dayOfWeek\": %s, \"startTime\": \"%s\", \"endTime\": \"%s\", \"maxCapacity\": %s, \"planningTemplateId\": \"%s\"}",
				DayOfWeek.FRIDAY.getValue(), START_TIME_VALUE, END_TIME_VALUE, MAX_CAPACITY, NOT_FOUND_PLANNING_TEMPLATE_ID);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void create() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);

		String requestBody = String.format(
				"{\"dayOfWeek\": %s, \"startTime\": \"%s\", \"endTime\": \"%s\", \"maxCapacity\": %s, \"planningTemplateId\": \"%s\"}",
				DayOfWeek.FRIDAY.getValue(), START_TIME_VALUE, END_TIME_VALUE, MAX_CAPACITY, planningTemplate.getId());

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
				.andExpect(header().exists("Location"));
	}

	@Test
	public void patchNonExisting() throws Exception {
		String requestBody = String.format("{\"dayOfWeek\": %s, \"startTime\": \"%s\", \"endTime\": \"%s\", \"maxCapacity\": %s}",
				DayOfWeek.FRIDAY.getValue(), START_TIME_VALUE, END_TIME_VALUE, MAX_CAPACITY);

		mockMvc.perform(patch(String.format("%s/%s", BASE_CONTROLLER_URL, NOT_FOUND_SLOT_TEMPLATE_ID)).content(requestBody)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	public void patchExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		String requestBody = String.format("{\"dayOfWeek\": %s, \"startTime\": \"%s\", \"endTime\": \"%s\", \"maxCapacity\": %s}",
				DayOfWeek.MONDAY.getValue(), START_TIME2_VALUE, END_TIME2_VALUE, MAX_CAPACITY2);

		mockMvc.perform(
				patch(String.format("%s/%s", BASE_CONTROLLER_URL, slotTemplate.getId())).content(requestBody).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNoContent());
	}

	@Test
	public void deleteNonExisting() throws Exception {
		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, NOT_FOUND_SLOT_TEMPLATE_ID))).andExpect(status().isNoContent());
	}

	@Test
	public void deleteExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, slotTemplate.getId()))).andExpect(status().isNoContent());
	}

	@Test
	public void deleteExistingWithSlots() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);
		slotRepository.save(slot);

		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, slotTemplate.getId()))).andExpect(status().isNoContent());
	}

}
