package dev.valora.slotservice.functional.test;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.ZoneId;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import dev.valora.slotservice.model.BookableResource;
import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.repository.BookableResourceRepository;
import dev.valora.slotservice.repository.PlanningRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class BookableResourceTests {

	private static final String BASE_CONTROLLER_URL = "/bookable-resources";
	private static final String BOOKABLE_RESOURCE_ID = "ref1";
	private static final String VALID_LABEL = "label";
	private static final String VALID_LABEL2 = "label2";
	private static final String VALID_TIME_ZONE = "Europe/Paris";
	private static final String VALID_TIME_ZONE2 = "Europe/Berlin";

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private BookableResourceRepository bookableResourceRepository;
	@Autowired
	private PlanningRepository planningRepository;

	private BookableResource bookableResource;
	private Planning planning;

	public BookableResourceTests() {
		bookableResource = new BookableResource();
		bookableResource.setId(BOOKABLE_RESOURCE_ID);
		bookableResource.setLabel(VALID_LABEL);
		bookableResource.setTimeZone(ZoneId.of(VALID_TIME_ZONE));

		planning = new Planning();
		planning.setBookableResource(bookableResource);
		planning.setLabel(VALID_LABEL);
	}

	@After
	public void tearDown() {
		planningRepository.deleteAll();
		bookableResourceRepository.deleteAll();
	}

	@Test
	public void findAllWithEmptyList() throws Exception {
		mockMvc.perform(get(BASE_CONTROLLER_URL)).andExpect(status().isOk()).andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
				.andExpect(jsonPath("$._embedded.bookableResourceResponseList", hasSize(0)));
	}

	@Test
	public void findAllWithOneBookableResource() throws Exception {
		bookableResourceRepository.save(bookableResource);

		mockMvc.perform(get(BASE_CONTROLLER_URL)).andExpect(status().isOk()).andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
				.andExpect(jsonPath("$._embedded.bookableResourceResponseList", hasSize(1)))
				.andExpect(jsonPath("$._embedded.bookableResourceResponseList[0].id").value(BOOKABLE_RESOURCE_ID))
				.andExpect(jsonPath("$._embedded.bookableResourceResponseList[0].label").value(VALID_LABEL))
				.andExpect(jsonPath("$._embedded.bookableResourceResponseList[0].timeZone").value(VALID_TIME_ZONE));
	}

	@Test
	public void findByIdNonExisting() throws Exception {
		mockMvc.perform(get(String.format("%s/%s", BASE_CONTROLLER_URL, BOOKABLE_RESOURCE_ID))).andExpect(status().isNotFound());
	}

	@Test
	public void findByIdExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);

		mockMvc.perform(get(String.format("%s/%s", BASE_CONTROLLER_URL, BOOKABLE_RESOURCE_ID))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8)).andExpect(jsonPath("$.id").value(BOOKABLE_RESOURCE_ID))
				.andExpect(jsonPath("$.label").value(VALID_LABEL)).andExpect(jsonPath("$.timeZone").value(VALID_TIME_ZONE));
	}

	@Test
	public void saveNonExisting() throws Exception {
		String requestBody = String.format("{\"label\": \"%s\", \"timeZone\": \"%s\"}", VALID_LABEL, VALID_TIME_ZONE);

		mockMvc.perform(
				put(String.format("%s/%s", BASE_CONTROLLER_URL, BOOKABLE_RESOURCE_ID)).content(requestBody).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
	}

	@Test
	public void saveExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);

		String requestBody = String.format("{\"label\": \"%s\", \"timeZone\": \"%s\"}", VALID_LABEL2, VALID_TIME_ZONE2);

		mockMvc.perform(
				put(String.format("%s/%s", BASE_CONTROLLER_URL, BOOKABLE_RESOURCE_ID)).content(requestBody).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNoContent());
	}

	@Test
	public void deleteNonExisting() throws Exception {
		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, BOOKABLE_RESOURCE_ID))).andExpect(status().isNoContent());
	}

	@Test
	public void deleteExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);

		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, BOOKABLE_RESOURCE_ID))).andExpect(status().isNoContent());
	}

	@Test
	public void deleteExistingWithPlannings() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);

		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, BOOKABLE_RESOURCE_ID))).andExpect(status().isNoContent());
	}

}
