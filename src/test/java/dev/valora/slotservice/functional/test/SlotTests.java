package dev.valora.slotservice.functional.test;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import dev.valora.slotservice.model.BookableResource;
import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.model.PlanningTemplate;
import dev.valora.slotservice.model.Reservation;
import dev.valora.slotservice.model.Slot;
import dev.valora.slotservice.model.SlotTemplate;
import dev.valora.slotservice.repository.BookableResourceRepository;
import dev.valora.slotservice.repository.PlanningRepository;
import dev.valora.slotservice.repository.PlanningTemplateRepository;
import dev.valora.slotservice.repository.ReservationRepository;
import dev.valora.slotservice.repository.SlotRepository;
import dev.valora.slotservice.repository.SlotTemplateRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class SlotTests {

	private static final String BASE_CONTROLLER_URL = "/slots";
	private static final String NOT_FOUND_SLOT_ID = "44444444-0000-0000-0000-444444444444";
	private static final UUID NOT_FOUND_SLOT_TEMPLATE_ID = UUID.fromString("44444444-0000-0000-0000-444444444444");
	private static final String BOOKABLE_RESOURCE_ID = "ref1";
	private static final String TIME_ZONE = "+02:00";
	private static final String START_DATE_TIME_VALUE = "2019-08-29T21:18:25.652Z";
	private static ZonedDateTime START_DATE_TIME;
	private static final String END_DATE_TIME_VALUE = "2019-09-07T21:18:25.652Z";
	private static ZonedDateTime END_DATE_TIME;
	private static final String START_DATE_TIME2_VALUE = "2019-09-01T21:18:25.652Z";
	private static ZonedDateTime START_DATE_TIME2;
	private static final ZonedDateTime END_DATE_TIME2 = null;
	private static final String START_DATE_TIME3_VALUE = "2019-09-05T12:00:00Z";
	private static ZonedDateTime START_DATE_TIME3;
	private static final String END_DATE_TIME3_VALUE = "2019-09-05T13:00:00Z";
	private static ZonedDateTime END_DATE_TIME3;
	private static final String START_TIME_VALUE = "21:00:00";
	private static LocalTime START_TIME;
	private static final String END_TIME_VALUE = "21:30:35";
	private static LocalTime END_TIME;
	private static final int MAX_CAPACITY = 10;
	private static final String START_TIME2_VALUE = "12:30:00";
	private static LocalTime START_TIME2;
	private static final String END_TIME2_VALUE = "21:30:35";
	private static LocalTime END_TIME2;
	private static final int MAX_CAPACITY2 = 2;
	private static final String START_TIME3_VALUE = "09:00:00";
	private static LocalTime START_TIME3;
	private static final String END_TIME3_VALUE = "10:00:00";
	private static LocalTime END_TIME3;
	private static final int MAX_CAPACITY3 = 3;
	private static final int MAX_CAPACITY4 = 4;
	private static final int MAX_CAPACITY5 = 5;
	private static final String START_DATE_TIME4_VALUE = "2019-09-02T" + START_TIME2_VALUE + TIME_ZONE;
	private static ZonedDateTime START_DATE_TIME4;
	private static final String START_DATE_TIME5_VALUE = "2019-09-09T11:30:00Z";
	private static ZonedDateTime START_DATE_TIME5;
	private static final String END_DATE_TIME5_VALUE = "2019-09-09T19:30:35Z";
	private static ZonedDateTime END_DATE_TIME5;

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private BookableResourceRepository bookableResourceRepository;
	@Autowired
	private PlanningRepository planningRepository;
	@Autowired
	private PlanningTemplateRepository planningTemplateRepository;
	@Autowired
	private SlotTemplateRepository slotTemplateRepository;
	@Autowired
	private SlotRepository slotRepository;
	@Autowired
	private ReservationRepository reservationRepository;

	private BookableResource bookableResource;
	private Planning planning;
	private PlanningTemplate planningTemplate;
	private SlotTemplate slotTemplate;
	private Planning planning2;
	private PlanningTemplate planningTemplate2;
	private SlotTemplate slotTemplate2;
	private PlanningTemplate planningTemplate3;
	private SlotTemplate slotTemplate3;
	private Slot slot;
	private Slot slot2;
	private Slot slot3;
	private Reservation reservation;

	public SlotTests() {
		START_DATE_TIME = ZonedDateTime.parse(START_DATE_TIME_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		END_DATE_TIME = ZonedDateTime.parse(END_DATE_TIME_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		START_DATE_TIME2 = ZonedDateTime.parse(START_DATE_TIME2_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		START_DATE_TIME3 = ZonedDateTime.parse(START_DATE_TIME3_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		END_DATE_TIME3 = ZonedDateTime.parse(END_DATE_TIME3_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		START_DATE_TIME4 = ZonedDateTime.parse(START_DATE_TIME4_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		START_DATE_TIME5 = ZonedDateTime.parse(START_DATE_TIME5_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		END_DATE_TIME5 = ZonedDateTime.parse(END_DATE_TIME5_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		START_TIME = LocalTime.parse(START_TIME_VALUE);
		END_TIME = LocalTime.parse(END_TIME_VALUE);
		START_TIME2 = LocalTime.parse(START_TIME2_VALUE);
		END_TIME2 = LocalTime.parse(END_TIME2_VALUE);
		START_TIME3 = LocalTime.parse(START_TIME3_VALUE);
		END_TIME3 = LocalTime.parse(END_TIME3_VALUE);

		bookableResource = new BookableResource();
		bookableResource.setId(BOOKABLE_RESOURCE_ID);
		bookableResource.setTimeZone(ZoneId.of("Europe/Paris"));

		planning = new Planning();
		planning.setBookableResource(bookableResource);

		planningTemplate = new PlanningTemplate();
		planningTemplate.setPlanning(planning);
		planningTemplate.setStartDateTime(START_DATE_TIME);
		planningTemplate.setEndDateTime(END_DATE_TIME);

		slotTemplate = new SlotTemplate();
		slotTemplate.setPlanningTemplate(planningTemplate);
		slotTemplate.setDayOfWeek(DayOfWeek.FRIDAY);
		slotTemplate.setMaxCapacity(MAX_CAPACITY);
		slotTemplate.setStartTime(START_TIME);
		slotTemplate.setEndTime(END_TIME);

		planning2 = new Planning();
		planning2.setBookableResource(bookableResource);

		planningTemplate2 = new PlanningTemplate();
		planningTemplate2.setPlanning(planning2);
		planningTemplate2.setStartDateTime(START_DATE_TIME2);
		planningTemplate2.setEndDateTime(END_DATE_TIME2);

		slotTemplate2 = new SlotTemplate();
		slotTemplate2.setPlanningTemplate(planningTemplate2);
		slotTemplate2.setDayOfWeek(DayOfWeek.MONDAY);
		slotTemplate2.setMaxCapacity(MAX_CAPACITY2);
		slotTemplate2.setStartTime(START_TIME2);
		slotTemplate2.setEndTime(END_TIME2);

		planningTemplate3 = new PlanningTemplate();
		planningTemplate3.setPlanning(planning2);
		planningTemplate3.setStartDateTime(START_DATE_TIME2);
		planningTemplate3.setEndDateTime(END_DATE_TIME2);

		slotTemplate3 = new SlotTemplate();
		slotTemplate3.setPlanningTemplate(planningTemplate3);
		slotTemplate3.setDayOfWeek(DayOfWeek.SUNDAY);
		slotTemplate3.setMaxCapacity(MAX_CAPACITY3);
		slotTemplate3.setStartTime(START_TIME3);
		slotTemplate3.setEndTime(END_TIME3);

		slot = new Slot();
		slot.setPlanning(planning);
		slot.setStartDateTime(START_DATE_TIME3);
		slot.setEndDateTime(END_DATE_TIME3);
		slot.setMaxCapacity(MAX_CAPACITY4);

		slot2 = new Slot();
		slot2.setPlanning(planning2);
		slot2.setStartDateTime(START_DATE_TIME4);
		slot2.setEndDateTime(START_DATE_TIME4);
		slot2.setSlotTemplate(slotTemplate2);
		slot2.setMaxCapacity(0);

		slot3 = new Slot();
		slot3.setPlanning(planning2);
		slot3.setStartDateTime(START_DATE_TIME5);
		slot3.setEndDateTime(END_DATE_TIME5);
		slot3.setMaxCapacity(MAX_CAPACITY5);

		reservation = new Reservation();
		reservation.setSlot(slot);
		reservation.setOwner("owner");
	}

	@After
	public void tearDown() {
		reservationRepository.deleteAll();
		slotRepository.deleteAll();
		slotTemplateRepository.deleteAll();
		planningTemplateRepository.deleteAll();
		planningRepository.deleteAll();
		bookableResourceRepository.deleteAll();
	}

	@Test
	public void findAllWithoutRequestParams() throws Exception {
		mockMvc.perform(get(BASE_CONTROLLER_URL)).andExpect(status().isBadRequest());
	}

	@Test
	public void findAllWithEmptyList() throws Exception {
		mockMvc.perform(get(BASE_CONTROLLER_URL).param("startDateTimeFrom", "2018-03-24T21:18:25.652Z")
				.param("startDateTimeTo", "2018-03-25T21:18:25.652Z").param("bookableResourceId", BOOKABLE_RESOURCE_ID)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8)).andExpect(jsonPath("$._embedded.slotResponseList", hasSize(0)));
	}

	@Test
	public void findAll() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);
		planningRepository.save(planning2);
		planningTemplateRepository.save(planningTemplate2);
		slotTemplateRepository.save(slotTemplate2);
		planningTemplateRepository.save(planningTemplate3);
		slotTemplateRepository.save(slotTemplate3);
		slotRepository.save(slot);
		slot2.setId(slotTemplate2.getId() + "20190902");
		slotRepository.save(slot2);
		slot3.setId(slotTemplate2.getId() + "20190909");
		slotRepository.save(slot3);

		mockMvc.perform(get(BASE_CONTROLLER_URL).param("startDateTimeFrom", "2019-08-30T19:18:25.652Z")
				.param("startDateTimeTo", "2019-10-06T08:30:25.652Z").param("bookableResourceId", BOOKABLE_RESOURCE_ID)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8)).andExpect(jsonPath("$._embedded.slotResponseList", hasSize(11)))

				.andExpect(jsonPath("$._embedded.slotResponseList[0].id").value(slot.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[0].planningId").value(planning.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[0].slotTemplateId").isEmpty())
				.andExpect(jsonPath("$._embedded.slotResponseList[0].maxCapacity").value(MAX_CAPACITY4))
				.andExpect(jsonPath("$._embedded.slotResponseList[0].startDateTime").value(START_DATE_TIME3_VALUE))
				.andExpect(jsonPath("$._embedded.slotResponseList[0].endDateTime").value(END_DATE_TIME3_VALUE))

				.andExpect(jsonPath("$._embedded.slotResponseList[1].id").value(slotTemplate2.getId() + "20190909"))
				.andExpect(jsonPath("$._embedded.slotResponseList[1].planningId").value(planning2.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[1].slotTemplateId").isEmpty())
				.andExpect(jsonPath("$._embedded.slotResponseList[1].maxCapacity").value(MAX_CAPACITY5))
				.andExpect(jsonPath("$._embedded.slotResponseList[1].startDateTime").value(START_DATE_TIME5_VALUE))
				.andExpect(jsonPath("$._embedded.slotResponseList[1].endDateTime").value(END_DATE_TIME5_VALUE))

				.andExpect(jsonPath("$._embedded.slotResponseList[2].id").value(slotTemplate.getId() + "20190906"))
				.andExpect(jsonPath("$._embedded.slotResponseList[2].planningId").value(planning.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[2].slotTemplateId").value(slotTemplate.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[2].maxCapacity").value(MAX_CAPACITY))
				.andExpect(jsonPath("$._embedded.slotResponseList[2].startDateTime").value("2019-09-06T" + START_TIME_VALUE + TIME_ZONE))
				.andExpect(jsonPath("$._embedded.slotResponseList[2].endDateTime").value("2019-09-06T" + END_TIME_VALUE + TIME_ZONE))

				.andExpect(jsonPath("$._embedded.slotResponseList[3].id").value(slotTemplate2.getId() + "20190916"))
				.andExpect(jsonPath("$._embedded.slotResponseList[3].planningId").value(planning2.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[3].slotTemplateId").value(slotTemplate2.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[3].maxCapacity").value(MAX_CAPACITY2))
				.andExpect(jsonPath("$._embedded.slotResponseList[3].startDateTime").value("2019-09-16T" + START_TIME2_VALUE + TIME_ZONE))
				.andExpect(jsonPath("$._embedded.slotResponseList[3].endDateTime").value("2019-09-16T" + END_TIME2_VALUE + TIME_ZONE))

				.andExpect(jsonPath("$._embedded.slotResponseList[4].id").value(slotTemplate2.getId() + "20190923"))
				.andExpect(jsonPath("$._embedded.slotResponseList[4].planningId").value(planning2.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[4].slotTemplateId").value(slotTemplate2.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[4].maxCapacity").value(MAX_CAPACITY2))
				.andExpect(jsonPath("$._embedded.slotResponseList[4].startDateTime").value("2019-09-23T" + START_TIME2_VALUE + TIME_ZONE))
				.andExpect(jsonPath("$._embedded.slotResponseList[4].endDateTime").value("2019-09-23T" + END_TIME2_VALUE + TIME_ZONE))

				.andExpect(jsonPath("$._embedded.slotResponseList[5].id").value(slotTemplate2.getId() + "20190930"))
				.andExpect(jsonPath("$._embedded.slotResponseList[5].planningId").value(planning2.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[5].slotTemplateId").value(slotTemplate2.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[5].maxCapacity").value(MAX_CAPACITY2))
				.andExpect(jsonPath("$._embedded.slotResponseList[5].startDateTime").value("2019-09-30T" + START_TIME2_VALUE + TIME_ZONE))
				.andExpect(jsonPath("$._embedded.slotResponseList[5].endDateTime").value("2019-09-30T" + END_TIME2_VALUE + TIME_ZONE))

				.andExpect(jsonPath("$._embedded.slotResponseList[6].id").value(slotTemplate3.getId() + "20190908"))
				.andExpect(jsonPath("$._embedded.slotResponseList[6].planningId").value(planning2.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[6].slotTemplateId").value(slotTemplate3.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[6].maxCapacity").value(MAX_CAPACITY3))
				.andExpect(jsonPath("$._embedded.slotResponseList[6].startDateTime").value("2019-09-08T" + START_TIME3_VALUE + TIME_ZONE))
				.andExpect(jsonPath("$._embedded.slotResponseList[6].endDateTime").value("2019-09-08T" + END_TIME3_VALUE + TIME_ZONE))

				.andExpect(jsonPath("$._embedded.slotResponseList[7].id").value(slotTemplate3.getId() + "20190915"))
				.andExpect(jsonPath("$._embedded.slotResponseList[7].planningId").value(planning2.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[7].slotTemplateId").value(slotTemplate3.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[7].maxCapacity").value(MAX_CAPACITY3))
				.andExpect(jsonPath("$._embedded.slotResponseList[7].startDateTime").value("2019-09-15T" + START_TIME3_VALUE + TIME_ZONE))
				.andExpect(jsonPath("$._embedded.slotResponseList[7].endDateTime").value("2019-09-15T" + END_TIME3_VALUE + TIME_ZONE))

				.andExpect(jsonPath("$._embedded.slotResponseList[8].id").value(slotTemplate3.getId() + "20190922"))
				.andExpect(jsonPath("$._embedded.slotResponseList[8].planningId").value(planning2.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[8].slotTemplateId").value(slotTemplate3.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[8].maxCapacity").value(MAX_CAPACITY3))
				.andExpect(jsonPath("$._embedded.slotResponseList[8].startDateTime").value("2019-09-22T" + START_TIME3_VALUE + TIME_ZONE))
				.andExpect(jsonPath("$._embedded.slotResponseList[8].endDateTime").value("2019-09-22T" + END_TIME3_VALUE + TIME_ZONE))

				.andExpect(jsonPath("$._embedded.slotResponseList[9].id").value(slotTemplate3.getId() + "20190929"))
				.andExpect(jsonPath("$._embedded.slotResponseList[9].planningId").value(planning2.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[9].slotTemplateId").value(slotTemplate3.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[9].maxCapacity").value(MAX_CAPACITY3))
				.andExpect(jsonPath("$._embedded.slotResponseList[9].startDateTime").value("2019-09-29T" + START_TIME3_VALUE + TIME_ZONE))
				.andExpect(jsonPath("$._embedded.slotResponseList[9].endDateTime").value("2019-09-29T" + END_TIME3_VALUE + TIME_ZONE))

				.andExpect(jsonPath("$._embedded.slotResponseList[10].id").value(slotTemplate3.getId() + "20191006"))
				.andExpect(jsonPath("$._embedded.slotResponseList[10].planningId").value(planning2.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[10].slotTemplateId").value(slotTemplate3.getId().toString()))
				.andExpect(jsonPath("$._embedded.slotResponseList[10].maxCapacity").value(MAX_CAPACITY3))
				.andExpect(jsonPath("$._embedded.slotResponseList[10].startDateTime").value("2019-10-06T" + START_TIME3_VALUE + TIME_ZONE))
				.andExpect(jsonPath("$._embedded.slotResponseList[10].endDateTime").value("2019-10-06T" + END_TIME3_VALUE + TIME_ZONE));
	}

	@Test
	public void findByIdWithExistingSlot() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		slotRepository.save(slot);

		mockMvc.perform(get(String.format("%s/%s", BASE_CONTROLLER_URL, slot.getId()))).andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(slot.getId().toString())).andExpect(jsonPath("$.planningId").value(planning.getId().toString()))
				.andExpect(jsonPath("$.slotTemplateId").isEmpty()).andExpect(jsonPath("$.maxCapacity").value(MAX_CAPACITY4))
				.andExpect(jsonPath("$.startDateTime").value(START_DATE_TIME3_VALUE))
				.andExpect(jsonPath("$.endDateTime").value(END_DATE_TIME3_VALUE));
	}

	@Test
	public void findByIdNotExistingSlotOrSlotTemplateAndWrongIdLength() throws Exception {
		mockMvc.perform(get(String.format("%s/%s", BASE_CONTROLLER_URL, NOT_FOUND_SLOT_ID))).andExpect(status().isNotFound());
	}

	@Test
	public void findByIdNotExistingSlotOrSlotTemplateAndValidIdLength() throws Exception {
		mockMvc.perform(get(String.format("%s/%s%s", BASE_CONTROLLER_URL, NOT_FOUND_SLOT_TEMPLATE_ID, "20190916"))).andExpect(status().isNotFound());
	}

	@Test
	public void findByIdNotExistingSlotOrSlotTemplateAndValidIdLengthAndWrongUuidFormat() throws Exception {
		mockMvc.perform(get(String.format("%s/%s", BASE_CONTROLLER_URL, "44444444-0000-0000-0000z44444444444420190916")))
				.andExpect(status().isNotFound());
	}

	@Test
	public void findByIdNotExistingSlotOrSlotTemplateAndValidIdLengthAndWrongDateTimeFormat() throws Exception {
		mockMvc.perform(get(String.format("%s/%s%s", BASE_CONTROLLER_URL, NOT_FOUND_SLOT_TEMPLATE_ID, "2019z916"))).andExpect(status().isNotFound());
	}

	@Test
	public void findByIdWithExistingSlotTemplateAndWrongDayOfWeek() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		mockMvc.perform(get(String.format("%s/%s%s", BASE_CONTROLLER_URL, slotTemplate.getId(), "20190831"))).andExpect(status().isNotFound());
	}

	@Test
	public void findByIdWithExistingSlotTemplateNullEndDateAndTooEarlyDate() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplate.setEndDateTime(null);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		mockMvc.perform(get(String.format("%s/%s%s", BASE_CONTROLLER_URL, slotTemplate.getId(), "20190823"))).andExpect(status().isNotFound());
	}

	@Test
	public void findByIdWithExistingSlotTemplateAndTooEarlyDate() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		mockMvc.perform(get(String.format("%s/%s%s", BASE_CONTROLLER_URL, slotTemplate.getId(), "20190823"))).andExpect(status().isNotFound());
	}

	@Test
	public void findByIdWithExistingSlotTemplateAndTooLateDate() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		mockMvc.perform(get(String.format("%s/%s%s", BASE_CONTROLLER_URL, slotTemplate.getId(), "20190911"))).andExpect(status().isNotFound());
	}

	@Test
	public void findByIdWithExistingSlotTemplate() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		mockMvc.perform(get(String.format("%s/%s%s", BASE_CONTROLLER_URL, slotTemplate.getId(), "20190830"))).andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(slotTemplate.getId() + "20190830"))
				.andExpect(jsonPath("$.planningId").value(planning.getId().toString())).andExpect(jsonPath("$.slotTemplateId").isEmpty())
				.andExpect(jsonPath("$.maxCapacity").value(MAX_CAPACITY))
				.andExpect(jsonPath("$.startDateTime").value("2019-08-30T" + START_TIME_VALUE + TIME_ZONE))
				.andExpect(jsonPath("$.endDateTime").value("2019-08-30T" + END_TIME_VALUE + TIME_ZONE));
	}

	@Test
	public void create() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);

		String requestBody = String.format("{\"startDateTime\": \"%s\", \"endDateTime\": \"%s\", \"maxCapacity\": %s, \"planningId\": \"%s\"}",
				START_DATE_TIME3_VALUE, END_DATE_TIME3_VALUE, MAX_CAPACITY4, planning.getId());

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
				.andExpect(header().exists("Location"));
	}

	@Test
	public void createNotExistingPlanning() throws Exception {
		String requestBody = String.format("{\"startDateTime\": \"%s\", \"endDateTime\": \"%s\", \"maxCapacity\": %s, \"planningId\": \"%s\"}",
				START_DATE_TIME3_VALUE, END_DATE_TIME3_VALUE, MAX_CAPACITY4, planning.getId());

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void saveNonExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);

		String requestBody = String.format("{\"startDateTime\": \"%s\", \"endDateTime\": \"%s\", \"maxCapacity\": %s, \"planningId\": \"%s\"}",
				START_DATE_TIME3_VALUE, END_DATE_TIME3_VALUE, MAX_CAPACITY4, planning.getId());

		mockMvc.perform(put(String.format("%s/%s", BASE_CONTROLLER_URL, slot.getId())).content(requestBody).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
	}

	@Test
	public void saveExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);
		slotRepository.save(slot);

		String requestBody = String.format("{\"startDateTime\": \"%s\", \"endDateTime\": \"%s\", \"maxCapacity\": %s, \"planningId\": \"%s\"}",
				START_DATE_TIME3_VALUE, END_DATE_TIME3_VALUE, MAX_CAPACITY4, planning.getId());

		mockMvc.perform(put(String.format("%s/%s", BASE_CONTROLLER_URL, slot.getId())).content(requestBody).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNoContent());
	}

	@Test
	public void saveNotExistingPlanning() throws Exception {
		String requestBody = String.format("{\"startDateTime\": \"%s\", \"endDateTime\": \"%s\", \"maxCapacity\": %s, \"planningId\": \"%s\"}",
				START_DATE_TIME3_VALUE, END_DATE_TIME3_VALUE, MAX_CAPACITY4, planning.getId());

		mockMvc.perform(put(String.format("%s/%s", BASE_CONTROLLER_URL, slot.getId())).content(requestBody).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void deleteNonExisting() throws Exception {
		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, slot.getId()))).andExpect(status().isNoContent());
	}

	@Test
	public void deleteExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);
		slot.setId(slotTemplate.getId() + "20190906");
		slot.setSlotTemplate(slotTemplate);
		slotRepository.save(slot);

		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, slot.getId()))).andExpect(status().isNoContent());
	}

	@Test
	public void deleteNonExistingSlotAndExistingSlotTemplate() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, slotTemplate.getId() + "20190906"))).andExpect(status().isNoContent());
	}

	@Test
	public void deleteNonExistingSlotAndExistingSlotTemplateWrongDate() throws Exception {
		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, slotTemplate.getId() + "201909z6"))).andExpect(status().isNoContent());
	}

	@Test
	public void deleteNonExistingSlotAndWrongSlotTemplateId() throws Exception {
		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, "3810ef71-b98b-4161g9e96a0a00263b183520190906")))
				.andExpect(status().isNoContent());
	}

	@Test
	public void deleteExistingWithReservations() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		slotRepository.save(slot);
		reservationRepository.save(reservation);

		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, slot.getId()))).andExpect(status().isBadRequest());
	}

}
