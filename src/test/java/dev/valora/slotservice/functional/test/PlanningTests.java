package dev.valora.slotservice.functional.test;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.ZoneId;
import java.util.UUID;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import dev.valora.slotservice.model.BookableResource;
import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.repository.BookableResourceRepository;
import dev.valora.slotservice.repository.PlanningRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class PlanningTests {

	private static final String BASE_CONTROLLER_URL = "/plannings";
	private static final String BOOKABLE_RESOURCE_ID = "ref1";
	private static final String BOOKABLE_RESOURCE_ID2 = "ref2";
	private static final String VALID_LABEL = "label";
	private static final String VALID_LABEL2 = "label2";
	private static final UUID NOT_FOUND_PLANNING_ID = UUID.fromString("44444444-0000-0000-0000-444444444444");

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private BookableResourceRepository bookableResourceRepository;
	@Autowired
	private PlanningRepository planningRepository;

	private BookableResource bookableResource;
	private Planning planning;
	private BookableResource bookableResource2;
	private Planning planning2;

	public PlanningTests() {
		bookableResource = new BookableResource();
		bookableResource.setId(BOOKABLE_RESOURCE_ID);
		bookableResource.setTimeZone(ZoneId.of("Europe/Paris"));

		planning = new Planning();
		planning.setBookableResource(bookableResource);
		planning.setLabel(VALID_LABEL);

		bookableResource2 = new BookableResource();
		bookableResource2.setId(BOOKABLE_RESOURCE_ID2);
		bookableResource2.setTimeZone(ZoneId.of("Europe/Berlin"));

		planning2 = new Planning();
		planning2.setBookableResource(bookableResource2);
		planning2.setLabel(VALID_LABEL2);
	}

	@After
	public void tearDown() {
		planningRepository.deleteAll();
		bookableResourceRepository.deleteAll();
	}

	@Test
	public void findAllWithEmptyList() throws Exception {
		mockMvc.perform(get(BASE_CONTROLLER_URL)).andExpect(status().isOk()).andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
				.andExpect(jsonPath("$._embedded.planningResponseList", hasSize(0)));
	}

	@Test
	public void findAllWithOnePlanning() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);

		mockMvc.perform(get(BASE_CONTROLLER_URL)).andExpect(status().isOk()).andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
				.andExpect(jsonPath("$._embedded.planningResponseList", hasSize(1)))
				.andExpect(jsonPath("$._embedded.planningResponseList[0].id").value(planning.getId().toString()))
				.andExpect(jsonPath("$._embedded.planningResponseList[0].label").value(VALID_LABEL))
				.andExpect(jsonPath("$._embedded.planningResponseList[0].bookableResourceId").value(bookableResource.getId()));
	}

	@Test
	public void findAllByBookableResourceId() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		bookableResourceRepository.save(bookableResource2);
		planningRepository.save(planning2);

		mockMvc.perform(get(BASE_CONTROLLER_URL).param("bookableResourceId", BOOKABLE_RESOURCE_ID2)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8)).andExpect(jsonPath("$._embedded.planningResponseList", hasSize(1)))
				.andExpect(jsonPath("$._embedded.planningResponseList[0].id").value(planning2.getId().toString()))
				.andExpect(jsonPath("$._embedded.planningResponseList[0].label").value(VALID_LABEL2))
				.andExpect(jsonPath("$._embedded.planningResponseList[0].bookableResourceId").value(bookableResource2.getId()));
	}

	@Test
	public void findByIdNonExisting() throws Exception {
		mockMvc.perform(get(String.format("%s/%s", BASE_CONTROLLER_URL, NOT_FOUND_PLANNING_ID))).andExpect(status().isNotFound());
	}

	@Test
	public void findByIdExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);

		mockMvc.perform(get(String.format("%s/%s", BASE_CONTROLLER_URL, planning.getId()))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8)).andExpect(jsonPath("$.id").value(planning.getId().toString()))
				.andExpect(jsonPath("$.label").value(VALID_LABEL)).andExpect(jsonPath("$.bookableResourceId").value(bookableResource.getId()));
	}

	@Test
	public void createWithoutBookableResourceId() throws Exception {
		String requestBody = String.format("{\"label\": \"%s\"}", VALID_LABEL);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void createWithNonExistingBookableResource() throws Exception {
		String requestBody = String.format("{\"label\": \"%s\", \"bookableResourceId\": \"%s\"}", VALID_LABEL, BOOKABLE_RESOURCE_ID);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void create() throws Exception {
		bookableResourceRepository.save(bookableResource);

		String requestBody = String.format("{\"label\": \"%s\", \"bookableResourceId\": \"%s\"}", VALID_LABEL, BOOKABLE_RESOURCE_ID);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
				.andExpect(header().exists("Location"));
	}

}
