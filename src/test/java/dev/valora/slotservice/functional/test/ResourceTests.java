package dev.valora.slotservice.functional.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.MediaTypes;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class ResourceTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void findAll() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isOk()).andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
				.andExpect(jsonPath("$._links.bookableResources.href").exists()).andExpect(jsonPath("$._links.plannings.href").exists())
				.andExpect(jsonPath("$._links.planningTemplates.href").exists()).andExpect(jsonPath("$._links.reservations.href").exists())
				.andExpect(jsonPath("$._links.slots.href").exists()).andExpect(jsonPath("$._links.slotTemplates.href").exists());
	}

}
