package dev.valora.slotservice.functional.test;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import dev.valora.slotservice.model.BookableResource;
import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.model.PlanningTemplate;
import dev.valora.slotservice.model.Reservation;
import dev.valora.slotservice.model.Slot;
import dev.valora.slotservice.model.SlotTemplate;
import dev.valora.slotservice.repository.BookableResourceRepository;
import dev.valora.slotservice.repository.PlanningRepository;
import dev.valora.slotservice.repository.PlanningTemplateRepository;
import dev.valora.slotservice.repository.ReservationRepository;
import dev.valora.slotservice.repository.SlotRepository;
import dev.valora.slotservice.repository.SlotTemplateRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class ReservationTests {

	private static final String BASE_CONTROLLER_URL = "/reservations";
	private static final String NOT_FOUND_SLOT_ID = "44444444-0000-0000-0000-444444444444";
	private static final UUID NOT_FOUND_SLOT_TEMPLATE_ID = UUID.fromString("44444444-0000-0000-0000-444444444444");
	private static final UUID NOT_FOUND_RESERVATION_ID = UUID.fromString("44444444-0000-0000-0000-444444444444");
	private static final String BOOKABLE_RESOURCE_ID = "ref1";
	private static final String START_DATE_TIME_VALUE = "2019-08-29T21:18:25.652Z";
	private static ZonedDateTime START_DATE_TIME;
	private static final String END_DATE_TIME_VALUE = "2019-09-07T21:18:25.652Z";
	private static ZonedDateTime END_DATE_TIME;
	private static final String START_DATE_TIME2_VALUE = "2019-09-01T21:18:25.652Z";
	private static ZonedDateTime START_DATE_TIME2;
	private static final ZonedDateTime END_DATE_TIME2 = null;
	private static final String START_DATE_TIME3_VALUE = "2019-09-05T12:00:00Z";
	private static ZonedDateTime START_DATE_TIME3;
	private static final String END_DATE_TIME3_VALUE = "2019-09-05T13:00:00Z";
	private static ZonedDateTime END_DATE_TIME3;
	private static final String START_TIME_VALUE = "21:00:00";
	private static LocalTime START_TIME;
	private static final String END_TIME_VALUE = "21:30:35";
	private static LocalTime END_TIME;
	private static final int MAX_CAPACITY = 10;
	private static final int MAX_CAPACITY4 = 4;
	private static final String VALID_OWNER = "test-owner";
	private static final String VALID_OWNER2 = "test-owner2";

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private BookableResourceRepository bookableResourceRepository;
	@Autowired
	private PlanningRepository planningRepository;
	@Autowired
	private PlanningTemplateRepository planningTemplateRepository;
	@Autowired
	private SlotTemplateRepository slotTemplateRepository;
	@Autowired
	private SlotRepository slotRepository;
	@Autowired
	private ReservationRepository reservationRepository;

	private BookableResource bookableResource;
	private Planning planning;
	private PlanningTemplate planningTemplate;
	private SlotTemplate slotTemplate;
	private Planning planning2;
	private PlanningTemplate planningTemplate2;
	private Slot slot;
	private Reservation reservation;
	private Slot slot2;
	private Reservation reservation2;

	public ReservationTests() {
		START_DATE_TIME = ZonedDateTime.parse(START_DATE_TIME_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		END_DATE_TIME = ZonedDateTime.parse(END_DATE_TIME_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		START_DATE_TIME2 = ZonedDateTime.parse(START_DATE_TIME2_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		START_DATE_TIME3 = ZonedDateTime.parse(START_DATE_TIME3_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		END_DATE_TIME3 = ZonedDateTime.parse(END_DATE_TIME3_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		START_TIME = LocalTime.parse(START_TIME_VALUE);
		END_TIME = LocalTime.parse(END_TIME_VALUE);

		bookableResource = new BookableResource();
		bookableResource.setId(BOOKABLE_RESOURCE_ID);
		bookableResource.setTimeZone(ZoneId.of("Europe/Paris"));

		planning = new Planning();
		planning.setBookableResource(bookableResource);

		planningTemplate = new PlanningTemplate();
		planningTemplate.setPlanning(planning);
		planningTemplate.setStartDateTime(START_DATE_TIME);
		planningTemplate.setEndDateTime(END_DATE_TIME);

		slotTemplate = new SlotTemplate();
		slotTemplate.setPlanningTemplate(planningTemplate);
		slotTemplate.setDayOfWeek(DayOfWeek.FRIDAY);
		slotTemplate.setMaxCapacity(MAX_CAPACITY);
		slotTemplate.setStartTime(START_TIME);
		slotTemplate.setEndTime(END_TIME);

		planningTemplate2 = new PlanningTemplate();
		planningTemplate2.setPlanning(planning2);
		planningTemplate2.setStartDateTime(START_DATE_TIME2);
		planningTemplate2.setEndDateTime(END_DATE_TIME2);

		slot = new Slot();
		slot.setPlanning(planning);
		slot.setStartDateTime(START_DATE_TIME3);
		slot.setEndDateTime(END_DATE_TIME3);
		slot.setMaxCapacity(MAX_CAPACITY4);

		reservation = new Reservation();
		reservation.setSlot(slot);
		reservation.setOwner(VALID_OWNER);
		reservation.setEndDateTime(END_DATE_TIME);

		slot2 = new Slot();
		slot2.setPlanning(planning);
		slot2.setStartDateTime(START_DATE_TIME3);
		slot2.setEndDateTime(END_DATE_TIME3);
		slot2.setMaxCapacity(MAX_CAPACITY4);

		reservation2 = new Reservation();
		reservation2.setSlot(slot2);
		reservation2.setOwner(VALID_OWNER2);
		reservation2.setEndDateTime(END_DATE_TIME3);
	}

	@After
	public void tearDown() {
		reservationRepository.deleteAll();
		slotRepository.deleteAll();
		slotTemplateRepository.deleteAll();
		planningTemplateRepository.deleteAll();
		planningRepository.deleteAll();
		bookableResourceRepository.deleteAll();
	}

	@Test
	public void findAllWithEmptyList() throws Exception {
		mockMvc.perform(get(BASE_CONTROLLER_URL)).andExpect(status().isOk()).andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
				.andExpect(jsonPath("$._embedded.reservationResponseList", hasSize(0)));
	}

	@Test
	public void findAllWithOneReservation() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		slotRepository.save(slot);
		reservationRepository.save(reservation);

		mockMvc.perform(get(BASE_CONTROLLER_URL)).andExpect(status().isOk()).andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
				.andExpect(jsonPath("$._embedded.reservationResponseList", hasSize(1)))
				.andExpect(jsonPath("$._embedded.reservationResponseList[0].id").value(reservation.getId().toString()))
				.andExpect(jsonPath("$._embedded.reservationResponseList[0].slotId").value(slot.getId().toString()))
				.andExpect(jsonPath("$._embedded.reservationResponseList[0].owner").value(VALID_OWNER))
				.andExpect(jsonPath("$._embedded.reservationResponseList[0].endDateTime").value(END_DATE_TIME_VALUE));
	}

	@Test
	public void findAllBySlotId() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		slotRepository.save(slot);
		reservationRepository.save(reservation);
		slotRepository.save(slot2);
		reservationRepository.save(reservation2);

		mockMvc.perform(get(BASE_CONTROLLER_URL).param("slotId", slot.getId())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8)).andExpect(jsonPath("$._embedded.reservationResponseList", hasSize(1)))
				.andExpect(jsonPath("$._embedded.reservationResponseList[0].id").value(reservation.getId().toString()))
				.andExpect(jsonPath("$._embedded.reservationResponseList[0].slotId").value(slot.getId().toString()))
				.andExpect(jsonPath("$._embedded.reservationResponseList[0].owner").value(VALID_OWNER))
				.andExpect(jsonPath("$._embedded.reservationResponseList[0].endDateTime").value(END_DATE_TIME_VALUE));
	}

	@Test
	public void findAllByOwner() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		slotRepository.save(slot);
		reservationRepository.save(reservation);
		slotRepository.save(slot2);
		reservationRepository.save(reservation2);

		mockMvc.perform(get(BASE_CONTROLLER_URL).param("owner", VALID_OWNER2)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8)).andExpect(jsonPath("$._embedded.reservationResponseList", hasSize(1)))
				.andExpect(jsonPath("$._embedded.reservationResponseList[0].id").value(reservation2.getId().toString()))
				.andExpect(jsonPath("$._embedded.reservationResponseList[0].slotId").value(slot2.getId().toString()))
				.andExpect(jsonPath("$._embedded.reservationResponseList[0].owner").value(VALID_OWNER2))
				.andExpect(jsonPath("$._embedded.reservationResponseList[0].endDateTime").value(END_DATE_TIME3_VALUE));
	}

	@Test
	public void findByIdNonExisting() throws Exception {
		mockMvc.perform(get(String.format("%s/%s", BASE_CONTROLLER_URL, NOT_FOUND_RESERVATION_ID))).andExpect(status().isNotFound());
	}

	@Test
	public void findByIdExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		slotRepository.save(slot);
		reservationRepository.save(reservation);

		mockMvc.perform(get(String.format("%s/%s", BASE_CONTROLLER_URL, reservation.getId()))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8)).andExpect(jsonPath("$.id").value(reservation.getId().toString()))
				.andExpect(jsonPath("$.slotId").value(slot.getId().toString())).andExpect(jsonPath("$.owner").value(VALID_OWNER));
	}

	@Test
	public void createWithExistingSlot() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		slotRepository.save(slot);

		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", slot.getId(), VALID_OWNER);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
				.andExpect(header().exists("Location"));
	}

	@Test
	public void createNotExistingSlotOrSlotTemplateAndWrongIdLength() throws Exception {
		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", NOT_FOUND_SLOT_ID, VALID_OWNER);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void createNotExistingSlotOrSlotTemplateAndValidIdLength() throws Exception {
		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", NOT_FOUND_SLOT_TEMPLATE_ID + "20190916", VALID_OWNER);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void createNotExistingSlotOrSlotTemplateAndValidIdLengthAndWrongUuidFormat() throws Exception {
		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", "44444444-0000-0000-0000z44444444444420190916", VALID_OWNER);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void createNotExistingSlotOrSlotTemplateAndValidIdLengthAndWrongDateTimeFormat() throws Exception {
		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", NOT_FOUND_SLOT_TEMPLATE_ID + "2019z916", VALID_OWNER);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void createWithExistingSlotTemplateAndWrongDayOfWeek() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", slotTemplate.getId() + "20190831", VALID_OWNER);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void createWithExistingSlotTemplateNullEndDateAndTooEarlyDate() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplate.setEndDateTime(null);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", slotTemplate.getId() + "20190823", VALID_OWNER);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void createWithExistingSlotTemplateAndTooEarlyDate() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", slotTemplate.getId() + "20190823", VALID_OWNER);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void createWithExistingSlotTemplateAndTooLateDate() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", slotTemplate.getId() + "20190911", VALID_OWNER);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void createWithExistingSlotTemplate() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", slotTemplate.getId() + "20190830", VALID_OWNER);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
				.andExpect(header().exists("Location"));
	}

	@Test
	public void createWithExistingFullSlot() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		slot.setMaxCapacity(1);
		slotRepository.save(slot);
		reservationRepository.save(reservation);

		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", slot.getId(), VALID_OWNER);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void updateNonExisting() throws Exception {
		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", slot.getId(), VALID_OWNER);

		mockMvc.perform(put(String.format("%s/%s", BASE_CONTROLLER_URL, NOT_FOUND_RESERVATION_ID)).content(requestBody)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	public void updateExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		slotRepository.save(slot);
		reservationRepository.save(reservation);

		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", slot.getId(), VALID_OWNER);

		mockMvc.perform(
				put(String.format("%s/%s", BASE_CONTROLLER_URL, reservation.getId())).content(requestBody).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNoContent());
	}

	@Test
	public void patchNonExisting() throws Exception {
		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", slot.getId(), VALID_OWNER);

		mockMvc.perform(patch(String.format("%s/%s", BASE_CONTROLLER_URL, NOT_FOUND_RESERVATION_ID)).content(requestBody)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	public void patchExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		slotRepository.save(slot);
		reservationRepository.save(reservation);

		String requestBody = String.format("{\"slotId\": \"%s\", \"owner\": \"%s\"}", slot.getId(), VALID_OWNER);

		mockMvc.perform(
				patch(String.format("%s/%s", BASE_CONTROLLER_URL, reservation.getId())).content(requestBody).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNoContent());
	}

	@Test
	public void deleteNonExisting() throws Exception {
		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, NOT_FOUND_RESERVATION_ID))).andExpect(status().isNoContent());
	}

	@Test
	public void deleteExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		slotRepository.save(slot);
		reservationRepository.save(reservation);

		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, reservation.getId()))).andExpect(status().isNoContent());
	}

}
