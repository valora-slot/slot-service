package dev.valora.slotservice.functional.test;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import dev.valora.slotservice.model.BookableResource;
import dev.valora.slotservice.model.Planning;
import dev.valora.slotservice.model.PlanningTemplate;
import dev.valora.slotservice.model.SlotTemplate;
import dev.valora.slotservice.repository.BookableResourceRepository;
import dev.valora.slotservice.repository.PlanningRepository;
import dev.valora.slotservice.repository.PlanningTemplateRepository;
import dev.valora.slotservice.repository.SlotTemplateRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class PlanningTemplateTests {

	private static final String BASE_CONTROLLER_URL = "/planning-templates";
	private static final String BOOKABLE_RESOURCE_ID = "ref1";
	private static final UUID NOT_FOUND_PLANNING_TEMPLATE_ID = UUID.fromString("44444444-0000-0000-0000-444444444444");
	private static final UUID NOT_FOUND_PLANNING_ID = UUID.fromString("44444444-0000-0000-0000-444444444444");
	private static final String START_DATE_TIME_VALUE = "2019-03-24T21:18:25.652Z";
	private static ZonedDateTime START_DATE_TIME;
	private static final String END_DATE_TIME_VALUE = "2019-03-25T21:18:25.652Z";
	private static ZonedDateTime END_DATE_TIME;
	private static final String START_DATE_TIME2_VALUE = "2019-03-26T21:18:25.652Z";
	private static ZonedDateTime START_DATE_TIME2;
	private static final String END_DATE_TIME2_VALUE = "2019-03-27T21:18:25.652Z";
	private static ZonedDateTime END_DATE_TIME2;
	private static final String START_TIME_VALUE = "21:00:00";
	private static LocalTime START_TIME;
	private static final String END_TIME_VALUE = "21:30:35";
	private static LocalTime END_TIME;

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private BookableResourceRepository bookableResourceRepository;
	@Autowired
	private PlanningRepository planningRepository;
	@Autowired
	private PlanningTemplateRepository planningTemplateRepository;
	@Autowired
	private SlotTemplateRepository slotTemplateRepository;

	private BookableResource bookableResource;
	private Planning planning;
	private PlanningTemplate planningTemplate;
	private SlotTemplate slotTemplate;
	private Planning planning2;
	private PlanningTemplate planningTemplate2;

	public PlanningTemplateTests() {
		START_DATE_TIME = ZonedDateTime.parse(START_DATE_TIME_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		END_DATE_TIME = ZonedDateTime.parse(END_DATE_TIME_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		START_DATE_TIME2 = ZonedDateTime.parse(START_DATE_TIME2_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		END_DATE_TIME2 = ZonedDateTime.parse(END_DATE_TIME2_VALUE, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		START_TIME = LocalTime.parse(START_TIME_VALUE);
		END_TIME = LocalTime.parse(END_TIME_VALUE);

		bookableResource = new BookableResource();
		bookableResource.setId(BOOKABLE_RESOURCE_ID);
		bookableResource.setTimeZone(ZoneId.of("Europe/Paris"));

		planning = new Planning();
		planning.setBookableResource(bookableResource);

		planningTemplate = new PlanningTemplate();
		planningTemplate.setPlanning(planning);
		planningTemplate.setStartDateTime(START_DATE_TIME);
		planningTemplate.setEndDateTime(END_DATE_TIME);

		slotTemplate = new SlotTemplate();
		slotTemplate.setPlanningTemplate(planningTemplate);
		slotTemplate.setDayOfWeek(DayOfWeek.FRIDAY);
		slotTemplate.setMaxCapacity(2);
		slotTemplate.setStartTime(START_TIME);
		slotTemplate.setEndTime(END_TIME);

		planning2 = new Planning();
		planning2.setBookableResource(bookableResource);

		planningTemplate2 = new PlanningTemplate();
		planningTemplate2.setPlanning(planning2);
		planningTemplate2.setStartDateTime(START_DATE_TIME2);
		planningTemplate2.setEndDateTime(END_DATE_TIME2);
	}

	@After
	public void tearDown() {
		slotTemplateRepository.deleteAll();
		planningTemplateRepository.deleteAll();
		planningRepository.deleteAll();
		bookableResourceRepository.deleteAll();
	}

	@Test
	public void findAllWithEmptyList() throws Exception {
		mockMvc.perform(get(BASE_CONTROLLER_URL)).andExpect(status().isOk()).andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
				.andExpect(jsonPath("$._embedded.planningTemplateResponseList", hasSize(0)));
	}

	@Test
	public void findAllWithOnePlanningTemplate() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);

		mockMvc.perform(get(BASE_CONTROLLER_URL)).andExpect(status().isOk()).andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
				.andExpect(jsonPath("$._embedded.planningTemplateResponseList", hasSize(1)))
				.andExpect(jsonPath("$._embedded.planningTemplateResponseList[0].id").value(planningTemplate.getId().toString()))
				.andExpect(jsonPath("$._embedded.planningTemplateResponseList[0].startDateTime").value(START_DATE_TIME_VALUE))
				.andExpect(jsonPath("$._embedded.planningTemplateResponseList[0].endDateTime").value(END_DATE_TIME_VALUE))
				.andExpect(jsonPath("$._embedded.planningTemplateResponseList[0].planningId").value(planning.getId().toString()));
	}

	@Test
	public void findAllByPlanningId() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		planningRepository.save(planning2);
		planningTemplateRepository.save(planningTemplate2);

		mockMvc.perform(get(BASE_CONTROLLER_URL).param("planningId", planning2.getId().toString())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
				.andExpect(jsonPath("$._embedded.planningTemplateResponseList", hasSize(1)))
				.andExpect(jsonPath("$._embedded.planningTemplateResponseList[0].id").value(planningTemplate2.getId().toString()))
				.andExpect(jsonPath("$._embedded.planningTemplateResponseList[0].startDateTime").value(START_DATE_TIME2_VALUE))
				.andExpect(jsonPath("$._embedded.planningTemplateResponseList[0].endDateTime").value(END_DATE_TIME2_VALUE))
				.andExpect(jsonPath("$._embedded.planningTemplateResponseList[0].planningId").value(planning2.getId().toString()));
	}

	@Test
	public void findByIdNonExisting() throws Exception {
		mockMvc.perform(get(String.format("%s/%s", BASE_CONTROLLER_URL, NOT_FOUND_PLANNING_TEMPLATE_ID))).andExpect(status().isNotFound());
	}

	@Test
	public void findByIdExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);

		mockMvc.perform(get(String.format("%s/%s", BASE_CONTROLLER_URL, planningTemplate.getId()))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8)).andExpect(jsonPath("$.id").value(planningTemplate.getId().toString()))
				.andExpect(jsonPath("$.startDateTime").value(START_DATE_TIME_VALUE)).andExpect(jsonPath("$.endDateTime").value(END_DATE_TIME_VALUE))
				.andExpect(jsonPath("$.planningId").value(planning.getId().toString()));
	}

	@Test
	public void createWithoutPlanningId() throws Exception {
		String requestBody = "{\"startDateTime\": null, \"endDateTime\": null}";

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void createWithNonExistingPlanning() throws Exception {
		String requestBody = String.format("{\"startDateTime\": \"2019-04-03T21:18:25.652Z\", \"endDateTime\": null, \"planningId\": \"%s\"}",
				NOT_FOUND_PLANNING_ID);

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void create() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);

		String requestBody = String.format("{\"startDateTime\": \"2019-04-03T21:18:25.652Z\", \"endDateTime\": null, \"planningId\": \"%s\"}",
				planning.getId());

		mockMvc.perform(post(BASE_CONTROLLER_URL).content(requestBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
				.andExpect(header().exists("Location"));
	}

	@Test
	public void updateNonExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);

		String requestBody = String.format("{\"startDateTime\": \"2019-04-03T21:18:25.652Z\", \"endDateTime\": null, \"planningId\": \"%s\"}",
				planning.getId());

		mockMvc.perform(put(String.format("%s/%s", BASE_CONTROLLER_URL, NOT_FOUND_PLANNING_TEMPLATE_ID)).content(requestBody)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	public void updateWithNonExistingPlanning() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);

		String requestBody = String.format("{\"startDateTime\": \"2019-04-03T21:18:25.652Z\", \"endDateTime\": null, \"planningId\": \"%s\"}",
				NOT_FOUND_PLANNING_ID);

		mockMvc.perform(put(String.format("%s/%s", BASE_CONTROLLER_URL, planningTemplate.getId())).content(requestBody)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void updateExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);

		String requestBody = String.format("{\"startDateTime\": \"2019-04-03T21:18:25.652Z\", \"endDateTime\": null, \"planningId\": \"%s\"}",
				planning.getId());

		mockMvc.perform(put(String.format("%s/%s", BASE_CONTROLLER_URL, planningTemplate.getId())).content(requestBody)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());
	}

	@Test
	public void patchNonExisting() throws Exception {
		String requestBody = String.format("{\"startDateTime\": \"2019-04-03T21:18:25.652Z\", \"endDateTime\": null}");

		mockMvc.perform(patch(String.format("%s/%s", BASE_CONTROLLER_URL, NOT_FOUND_PLANNING_TEMPLATE_ID)).content(requestBody)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	public void patchExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);

		String requestBody = String.format("{\"startDateTime\": \"2019-04-03T21:18:25.652Z\", \"endDateTime\": null}");

		mockMvc.perform(patch(String.format("%s/%s", BASE_CONTROLLER_URL, planningTemplate.getId())).content(requestBody)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());
	}

	@Test
	public void deleteNonExisting() throws Exception {
		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, NOT_FOUND_PLANNING_TEMPLATE_ID))).andExpect(status().isNoContent());
	}

	@Test
	public void deleteExisting() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);

		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, planningTemplate.getId()))).andExpect(status().isNoContent());
	}

	@Test
	public void deleteExistingWithSlotTemplates() throws Exception {
		bookableResourceRepository.save(bookableResource);
		planningRepository.save(planning);
		planningTemplateRepository.save(planningTemplate);
		slotTemplateRepository.save(slotTemplate);

		mockMvc.perform(delete(String.format("%s/%s", BASE_CONTROLLER_URL, planningTemplate.getId()))).andExpect(status().isNoContent());
	}

}
